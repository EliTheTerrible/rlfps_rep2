﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BunkerDoorController : MonoBehaviour
{
    public Animator doorAnimator;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Player")
            return;
        LevelManager.current.inExitZone = true;
        doorAnimator.ResetTrigger("player_out");
        doorAnimator.SetTrigger("player_in");
    }
    private void OnTriggerExit(Collider other)
    {        
        if (other.gameObject.tag != "Player")
            return;
        LevelManager.current.inExitZone = false;
        doorAnimator.ResetTrigger("player_in");
        doorAnimator.SetTrigger("player_out");

    }
}
