﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HubManager : LevelManager
{    
    public override void OnEnable()
    {
        if(current != this)
        {
            if (current != null) {
                Destroy(current.gameObject);
            }
            current = this;
        }                
    }

    public override void Update()
    {        
        if (inExitZone && Input.GetKeyDown(KeyCode.Return))
        {
            RunManager.current.StartRun();
        }
    }
}
