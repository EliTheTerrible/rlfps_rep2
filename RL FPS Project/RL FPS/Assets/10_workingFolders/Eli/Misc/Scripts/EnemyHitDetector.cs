﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHitDetector : MonoBehaviour
{
    EnemyController controller;
    public enum BodyPart { Head, Torso, LeftLeg, RightLeg, LeftArm, RightArm }
    [SerializeField] List<Collider> torso, leftLeg, rightLeg, leftArm, rightArm, head;
    [SerializeField] WaitForSeconds despawnDelay;
    List<Collider> hitBoxes = new List<Collider>();    

    public void Initialize(EnemySettings _settings)
    {
        despawnDelay = new WaitForSeconds(_settings.DespawnTime);
        if (controller == null)
            controller = GetComponent<EnemyController>();
        hitBoxes.AddRange(head);
        hitBoxes.AddRange(torso);
        hitBoxes.AddRange(leftArm);
        hitBoxes.AddRange(rightArm);
        hitBoxes.AddRange(leftLeg);
        hitBoxes.AddRange(rightLeg);
        foreach (Collider hitbox in hitBoxes)
        {
            var newHitBox = hitbox.gameObject.AddComponent<HitBox>();
            newHitBox.Initialize(this);
            if (torso.Contains(hitbox))
            {
                newHitBox.part = BodyPart.Torso;
            }
            else if (leftArm.Contains(hitbox))
            {
                newHitBox.part = BodyPart.LeftArm;
            }
            else if (rightArm.Contains(hitbox))
            {
                newHitBox.part = BodyPart.RightArm;
            }
            else if (leftLeg.Contains(hitbox))
            {
                newHitBox.part = BodyPart.LeftLeg;
            }
            else if (rightLeg.Contains(hitbox))
            {
                newHitBox.part = BodyPart.RightLeg;
            }
            else if (head.Contains(hitbox))
            {
                newHitBox.part = BodyPart.Head;
            }
        }
    }

    public void RecieveDamgeIn(BodyPart _hitPart)
    {
        controller.TakeDamage(10f, _hitPart);
        
    }

    public IEnumerator Despawn()
    {
        yield return despawnDelay;
        foreach(Collider hitbox in hitBoxes) 
            hitbox.enabled = false;
        yield return despawnDelay;
        gameObject.SetActive(false);
    }
}
