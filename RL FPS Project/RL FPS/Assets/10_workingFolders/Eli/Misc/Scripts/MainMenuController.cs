﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    public void StartGame(string _startingSceneName)
    {
        SceneTransitionController.current.StartTransion(_startingSceneName, null);
    }
}
