﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    public static PlayerController current;
    public float speed;    
    Transform _tranform, cameraTransform;
    Rigidbody playerRB;


    private void OnEnable()
    {
        _tranform = transform;
        if(current != this)
        {
            if(current != null)
            {
                DestroyImmediate(current.gameObject);
            }
            current = this;
        }
        if(cameraTransform == null)
        {
            cameraTransform = Camera.main.transform;
        }
        if(playerRB == null)
        {
            playerRB = GetComponent<Rigidbody>();
        }
    }
    
    public void Update()
    {        
        float walkInputVertical = Input.GetAxis("Vertical");
        float lookInputHorizontal = Input.GetAxis("Mouse X");
        float lookInputVertical = Input.GetAxis("Mouse Y");
        _tranform.Rotate(0f, lookInputHorizontal, 0f);
        cameraTransform.Rotate(lookInputVertical, 0f, 0f);
        Debug.Log(cameraTransform.localEulerAngles.x.ToString("F2"));
        playerRB.AddForce(_tranform.forward * walkInputVertical * speed);
        
    }
}
