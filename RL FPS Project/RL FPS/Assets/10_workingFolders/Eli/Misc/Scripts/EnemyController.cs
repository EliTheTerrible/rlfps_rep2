﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using System.Collections.Generic;

public class EnemyController : MonoBehaviour
{   
    public enum State { idle, patroling, chasing}

    [SerializeField] Transform Walkpoint;
    [SerializeField] Transform player;
    [SerializeField] Transform _transform;

    [Header("Local Refrencess")]
    [SerializeField] EnemySettings settings;
    [SerializeField] NavMeshAgent agent;
    [SerializeField] EnemyHealthController healthController;
    [SerializeField] EnemyHitDetector hitDetector;
    [SerializeField] Transform eyes;
    [SerializeField] LayerMask GroundM, PlayerM;
    [SerializeField] GameObject Projectile;

    [Header("Exposed Behaviour state")]
    [SerializeField]bool ready = false;
    [SerializeField] State currentState;
    [SerializeField] NavMeshPathStatus pathState; 
    [SerializeField] public float Health;
    [SerializeField] bool playerInSightRange;
    [SerializeField] bool playerSpoted;
    [SerializeField] bool playerInAttackRange;    
    [SerializeField] bool attacking;
    [SerializeField] bool alreadyAttacked;
    [SerializeField] bool linking = false;
    [SerializeField] float fowardForce;
    [SerializeField] float upwardForce;
    [SerializeField] Vector3 walkPoint;
    [SerializeField] List<Vector3> sightData;
        
    [Header("Animation Settings")]
    public Animator animator;   
    [Range(-1f,1f)]public float v_right;
    [Range(-1f,3f)]public float v_forward;
    public void OnEnable()
    {
        StopAllCoroutines();
        StartCoroutine(Initialize());
    }

    public EnemySettings GetSettings()
    {
        return settings;
    }
    public void Spawn(EnemySettings _settings)
    {
        StopAllCoroutines();
        settings = _settings;
        StartCoroutine(Initialize());
    }
    public void DisableControllers()
    {
        agent.enabled = false;
        StartCoroutine(hitDetector.Despawn());
    }
    IEnumerator Initialize()
    {        
        ready = false;
        currentState = State.idle;
        yield return new WaitUntil(() => LevelManager.current != null);
        if (!LevelManager.current.activeEnemies.ContainsKey(gameObject))
            LevelManager.current.activeEnemies.Add(gameObject, this);
        if (_transform == null)
            _transform = transform;
        if (player == null)
            player = LevelManager.current.player.transform;
        if (healthController == null)
            healthController = gameObject.AddComponent<EnemyHealthController>();
        if (!TryGetComponent<EnemyHitDetector>(out hitDetector))
        {
            Debug.Log("no hit detector found!");
            yield break;
        }
        yield return new WaitUntil(() => LevelGenerator.current != null);       
        if(!TryGetComponent<NavMeshAgent>(out agent))
            agent = gameObject.AddComponent<NavMeshAgent>();
        if(settings != null)
        {
            healthController.Initialize(settings);
            hitDetector.Initialize(settings);
            agent.speed = settings.speed;
            agent.acceleration = settings.acceleration;
            agent.angularSpeed = settings.angularSpeed;
            agent.radius = settings.radius;
            agent.autoBraking = settings.autoBraking;
            StartCoroutine(SpotPlayer());
            StartCoroutine(Patrol());
            StartCoroutine(UpdateAgentSettings());
            ready = true;
        }
        yield return null;
    }
    
    private void Update()
    {
        #region failsafes and manual control
        if (settings.manualControl) {
            animator.SetFloat("v_forward", v_forward);
            animator.SetFloat("v_right", v_right);
            return;
        }

        if (!ready || settings == null) return;
        
        if (!LevelGenerator.current.navmeshReady)
            return;
        #endregion
        pathState = agent.pathStatus;
        Walkpoint.position = walkPoint;
        if (Vector3.Distance(_transform.position, player.position) >= settings.minDetectionRange)
            playerSpoted = false;

        playerInAttackRange = Vector3.Distance(_transform.position, player.position) <= settings.attackRange;

        Patroling();
        
        UpdateAnimatorVelocity();
    }
    private void FixedUpdate()
    {
        #region Failsafes
        if (agent == null)
            return;
        if (agent.enabled == false)
            return;
        #endregion

        if (agent.isOnOffMeshLink && !linking)
        {
            linking = true;
            agent.speed = settings.speed;
        }
        if(!agent.isOnOffMeshLink && linking)
        {
            linking = false;
            agent.speed = settings.speed;
        }
    }
    public void UpdateAnimatorVelocity()
    {
        var realativeVelocity = _transform.InverseTransformVector(agent.velocity);
        animator.SetFloat("v_forward", realativeVelocity.z);
        animator.SetFloat("v_right", realativeVelocity.x);
    }

    void Patroling()
    {                
        if(Vector3.Distance(_transform.position, walkPoint) <= settings.stopingDistance)
        {
            currentState = State.idle;
        }
    }    
    void Chasing()
    {      
        agent.SetDestination(player.position);
    }
    void Attacking()
    {
        //agent.SetDestination(transform.position);
        agent.isStopped = true;
        transform.LookAt(player);

        // Attack Specification
        Rigidbody rb = Instantiate(Projectile, transform.position, Quaternion.identity).GetComponent<Rigidbody>();

        rb.AddForce(transform.forward * fowardForce, ForceMode.Impulse);
        rb.AddForce(transform.up * upwardForce, ForceMode.Impulse);

        if (!alreadyAttacked) {
            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), settings.attackRate);
        }
    }
    void ResetAttack()
    {
        alreadyAttacked = false;
    }
        public void TakeDamage(float _damage , EnemyHitDetector.BodyPart _part)
    {
        
        switch(_part)
        {
            case EnemyHitDetector.BodyPart.Torso:
                {
                    healthController.TakeTorsoDamage(_damage);
                    animator.SetTrigger("hit_torso");
                    break;
                }
            case EnemyHitDetector.BodyPart.LeftArm:
                {
                    healthController.TakeLimbDamage(_damage);
                    animator.SetTrigger("hit_leftArm");
                    break;
                }
            case EnemyHitDetector.BodyPart.RightArm:
                {
                    healthController.TakeLimbDamage(_damage);
                    animator.SetTrigger("hit_rightArm");
                    break;
                }
            case EnemyHitDetector.BodyPart.LeftLeg:
                {
                    healthController.TakeLimbDamage(_damage);
                    animator.SetTrigger("hit_leftLeg");
                    break;
                }
            case EnemyHitDetector.BodyPart.RightLeg:
                {
                    healthController.TakeLimbDamage(_damage);
                    animator.SetTrigger("hit_rightLeg");
                    break;
                }
            case EnemyHitDetector.BodyPart.Head:
                {
                    healthController.TakeHeadDamage(10f);
                    break;
                }
        }
    }        
    IEnumerator Patrol()
    {
        while(enabled)
        {
            while (currentState == State.idle)
            {
                yield return null;                
                NavMeshHit navHit;
                RaycastHit rayHit;
                var _randomPoint = _transform.position + new Vector3(Random.Range(-settings.roamRange, settings.roamRange), 0f, Random.Range(-settings.roamRange, settings.roamRange));
                if (Physics.Raycast(_randomPoint, Vector3.down, out rayHit, Mathf.Infinity))
                {
                    if (NavMesh.SamplePosition((rayHit.point), out navHit, 1f, NavMesh.AllAreas))
                    {
                        currentState = State.patroling;
                        walkPoint = rayHit.point;                        
                        agent.SetDestination(walkPoint);

                    }
                    else
                        continue;
                }
                else Health = 0f;
            }
            yield return null;
            
            if (agent.pathStatus == NavMeshPathStatus.PathPartial)
                currentState = State.idle;
        }
    }
    IEnumerator SpotPlayer()
    {
        while(enabled) {
            yield return new WaitUntil(() => LevelGenerator.current != null);
            if (!playerSpoted) {
                for(int i = 0; i < settings.spotDensity; ++i) {
                    var _realativeDir = (-settings.fov / 2) + (i * (settings.fov / settings.spotDensity));                
                    eyes.localRotation = Quaternion.Euler(0f, _realativeDir, 0f);
                    RaycastHit _spotChek = new RaycastHit();
                    if(Physics.Raycast(eyes.position, eyes.forward, out _spotChek, settings.sightRange)) { 
                        Debug.DrawRay(eyes.position, eyes.forward * _spotChek.distance, Color.red);
                        if(_spotChek.transform.tag == "Player") {
                            playerSpoted = true;
                        }
                    }
                    else
                    {

                    }
                }
            }
            yield return new WaitForSeconds(1/ settings.playerSpotsPreSec);
        }
    }
    IEnumerator UpdateAgentSettings()
    {
        while(enabled) {
            yield return new WaitUntil(() => LevelGenerator.current != null);
            if (agent.speed != settings.speed) {
                agent.speed = settings.speed;
            }
            if (agent.acceleration != settings.acceleration) {
                agent.acceleration = settings.acceleration;
            }
            if (agent.angularSpeed != settings.angularSpeed) {
                agent.angularSpeed = settings.angularSpeed;
            }
            if (agent.radius != settings.radius) {
                agent.radius = settings.radius;
            }
            if (agent.autoBraking != settings.autoBraking) {
                agent.autoBraking = settings.autoBraking;
            }
            yield return null;
        }
    }
    
    private void OnDrawGizmosSelected()
    {
        /*
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, settings.attackRange);

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, settings.sightRange);
        */
    }
}
