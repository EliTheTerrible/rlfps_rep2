﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionUpdater : MonoBehaviour
{
            
    [SerializeField] SegmentCell cell;
    private void OnTriggerEnter(Collider other)
    {
        if (cell == null)
            cell = GetComponentInParent<SegmentCell>();
        if (cell == null)
        {
            Debug.Log("Can't find cell =[");
            return;
        }        
        if(other.transform.tag == "Player")
        {
            cell.room.visible = true;
        }
        if(other.transform.root.tag == "Enemy") {            
            if (!LevelManager.current.activeEnemies.ContainsKey(other.gameObject)) {
                StartCoroutine(DelayedAssignment(other.gameObject));
                return;
            }            
        }
    }
    
    IEnumerator DelayedAssignment(GameObject other)
    {
        while(!LevelManager.current.activeEnemies.ContainsKey(other)) {            
            yield return null;
        }       
    }
}


