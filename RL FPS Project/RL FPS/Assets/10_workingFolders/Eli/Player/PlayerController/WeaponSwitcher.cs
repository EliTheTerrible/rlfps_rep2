﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponSwitcher : MonoBehaviour
{    
    Dictionary<WeaponAnimationLayer, Transform> weaponAnimationTranformsDictionary = new Dictionary<WeaponAnimationLayer, Transform>();
    Dictionary<string, RectTransform> availableIcons = new Dictionary<string, RectTransform>();
    Dictionary<WeaponCategory, List<WeaponIdentifier>> availableWeapons = new Dictionary<WeaponCategory, List<WeaponIdentifier>>();

    public static WeaponSwitcher current;
    public Transform[] weaponAnimationTransforms;
    public Transform initialWeapons;
    public RectTransform selectedWeaponIconHolder, nextWeaponPreviewIconHolder, previousWeaponPreviewIconHolder;
    public bool mouseScroll = false;
    public float scrollPreviewDuration = 2f;
    public float scrollActivationDuration = 0.3f;
    public float startOffsetX = 200f;
    public float startOffsetY = 300f;    
    float timer = 0f;
    RectTransform selectedWeaponIcon, nextWeaponPreviewIcon, previousWeaponPreviewIcon;
    WeaponIdentifier currentWeapon = null;
    [SerializeField] bool isScrolling;
    [SerializeField] bool showInfo;
    
    void OnEnable()
    {
        if (current != this)
        {
            if (current != null)
            {
                Destroy(current.gameObject);
            }
            current = this;
        }
        else
        {
            return;
        }

        if (initialWeapons.childCount != 0)
        {
            IndexExistingWeapons();
            SortAvailableWeapons();


            var _defaultCategory = WeaponCategory.Unarmed;                        
            if (availableWeapons.ContainsKey(_defaultCategory))
            {
                CycleCategoryStart(availableWeapons[_defaultCategory]);
                UpdateSelectedIcon();
                UpdateAnimationController();
            }
        }
        if (nextWeaponPreviewIconHolder != null)
            nextWeaponPreviewIconHolder.parent.gameObject.SetActive(false);
        if (previousWeaponPreviewIconHolder != null)
            previousWeaponPreviewIconHolder.parent.gameObject.SetActive(false);
    }

    #region Initialization Methods

    private void CreateWeaponDictionary()
    {
        
    }
    private void SortAvailableWeapons()
    {
        // move weapons in hierarchy to become children of their respective animation placeholders
        // might be obsalete

        // create a refrence dictinary that links between the animated traforms and the available weapons, without the Unarmed layer
        foreach(WeaponAnimationLayer _newLayer in WeaponIdentifier.availableLayers())
        {
            var _tranformIndex = _newLayer.LayerTranformIndex();
            if (_tranformIndex < 0)
                continue;
            weaponAnimationTranformsDictionary.Add(_newLayer, weaponAnimationTransforms[_tranformIndex]);
        }

        // Move each weapon to the appropriate transform
        foreach (WeaponCategory _weaponCategory in availableWeapons.Keys)
        {           
            foreach (WeaponIdentifier _weaponToSort in availableWeapons[_weaponCategory])
            {
                if(weaponAnimationTranformsDictionary.ContainsKey(_weaponToSort.animationLayer))
                {
                    _weaponToSort.transform.parent = weaponAnimationTranformsDictionary[_weaponToSort.animationLayer];
                    _weaponToSort.transform.localPosition = Vector3.zero;
                    _weaponToSort.transform.localRotation = Quaternion.identity;    
                    _weaponToSort.gameObject.SetActive(false);
                }
            }
        }
    }
    private void IndexExistingWeapons()
    {
        // add default unarmed entry
        availableWeapons.Add(WeaponCategory.Unarmed, new List<WeaponIdentifier>());

        // Create a dictionary of weapon in storage and sort in by weapon category
        for (int i = 0; i < initialWeapons.childCount; i++)
        {
            var _weapon = initialWeapons.GetChild(i).GetComponent<WeaponIdentifier>();
            
            // check if gameobject is a valid weapon
            if (_weapon == null)
                continue;

            // check if a key for the current weapon category already exist and add current weapon to it
            var _weaponType = _weapon.category;            
            if (availableWeapons.ContainsKey(_weaponType))
            {
                availableWeapons[_weaponType].Add(_weapon);
            }
            // create a new key and add current weapon to it
            else
            {
                var newWeaponList = new List<WeaponIdentifier>();
                availableWeapons.Add(_weaponType, newWeaponList);
                availableWeapons[_weaponType].Add(_weapon);
            }
 
            // ensure weapon is off
            _weapon.gameObject.SetActive(false);

        }
    }
    #endregion

    void Update()
    {
        var _active = initialWeapons != null && !PauseController.current.pause;
        if (_active)
        {
            if (mouseScroll)
                CheckMousescrollSwitch();

            CheckAlphanumaricSwitch();
        }        
    }
    IEnumerator ShowScrollPreview()
    {
        nextWeaponPreviewIconHolder.parent.gameObject.SetActive(true);
        previousWeaponPreviewIconHolder.parent.gameObject.SetActive(true);

        var startPositionNext = selectedWeaponIconHolder.parent.position + new Vector3(startOffsetX, 150f, 0f);
        var startPositionPrevious = selectedWeaponIconHolder.parent.position + new Vector3(-300f, -startOffsetY, 0f);
        var endPositionRefrence = selectedWeaponIconHolder.parent.position;

        float _timeElapse = 0f;


        nextWeaponPreviewIconHolder.parent.position = startPositionNext;
        previousWeaponPreviewIconHolder.parent.position = startPositionPrevious;

        while (_timeElapse <= scrollActivationDuration)
        {
            _timeElapse += Time.deltaTime;
            var _nextPrevieUpdatedPosition = Mathf.Lerp(startPositionNext.x, endPositionRefrence.x, _timeElapse / scrollActivationDuration);
            var _previousPrevieUpdatedPosition = Mathf.Lerp(startPositionPrevious.y, endPositionRefrence.y, _timeElapse / scrollActivationDuration);
            nextWeaponPreviewIconHolder.parent.position = new Vector3(_nextPrevieUpdatedPosition, selectedWeaponIconHolder.position.y + 150f, 0f);
            previousWeaponPreviewIconHolder.parent.position = new Vector3(selectedWeaponIconHolder.position.x - 300f, _previousPrevieUpdatedPosition, 0f);
            yield return null;
        }
        while (isScrolling)
        {

            yield return null;
        }
        _timeElapse = 0f;
        while (!isScrolling && _timeElapse <= scrollActivationDuration)
        {
            if (isScrolling)
                _timeElapse = 0f;
            _timeElapse += Time.deltaTime;
            var t = _timeElapse / scrollActivationDuration;
            var _nextPrevieUpdatedPosition = Mathf.Lerp(endPositionRefrence.x, startPositionNext.x, t);
            var _previousPrevieUpdatedPosition = Mathf.Lerp(endPositionRefrence.y, startPositionPrevious.y, t);

            nextWeaponPreviewIconHolder.parent.position = new Vector3(_nextPrevieUpdatedPosition, selectedWeaponIconHolder.position.y + 150f, 0f);
            previousWeaponPreviewIconHolder.parent.position = new Vector3(selectedWeaponIconHolder.position.x - 300f, _previousPrevieUpdatedPosition, 0f);
            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }

    public WeaponIdentifier GetCurrentWeapon()
    {
        return currentWeapon;
    }

    public string GetcurrentWeaponAnimationName()
    {
        if (currentWeapon != null)
            return currentWeapon.animationLayer.ToString();
        else
            return "-1";

    }


    #region weapon selection and switching mehthods
    private void CheckMousescrollSwitch()
    {

        var _mouseScrollInput = Input.GetAxisRaw("Mouse ScrollWheel");
        var _currentWeaponCategoryIndex = currentWeapon == null ? 0 : (int)currentWeapon.animationLayer;        
        var _currentWeaponIndexInCategory = currentWeapon == null ? 0 : GetIndexInCategory();

        if (_mouseScrollInput != 0f)
        {
            // Select Next Weapon
            if (_mouseScrollInput > 0)
            {
                // Switch To the start Next Category                
                if (currentWeapon == null)
                {
                    var _nextCategoryIndex = (_currentWeaponCategoryIndex + 1) % availableWeapons.Count;
                    var _nextType = (WeaponCategory)_nextCategoryIndex;
                    CycleCategoryStart(availableWeapons[_nextType]);
                }                
                else if (_currentWeaponIndexInCategory == availableWeapons[currentWeapon.category].Count - 1)
                {
                    var _nextCategoryIndex = (_currentWeaponCategoryIndex + 1) % availableWeapons.Count;
                    var _nextType = (WeaponCategory)_nextCategoryIndex;                    
                    CycleCategoryStart(availableWeapons[_nextType]);
                }
                // swith to next weapon
                else
                {                    
                    CycleCategoryNext(availableWeapons[(WeaponCategory)1]);
                }
                UpdateScrollPreviewIcons();
                UpdateSelectedIcon();
                UpdateAnimationController();
            }
        
            // Select Previous Weapon
            else if (_mouseScrollInput < 0)
            {
                // Swithc To Last of Previous Category
                if (currentWeapon == null) {
                    var _previousCategoryIndex = WeaponCategory.Shotgun;
                    var _previousCategory = (WeaponCategory)_previousCategoryIndex;
                    CycleCategortyLast(availableWeapons[_previousCategory]);
                }
                else if (_currentWeaponIndexInCategory == 0)
                {
                    var _previousCategoryIndex = ((int)currentWeapon.animationLayer - 1);
                    if (_previousCategoryIndex < 0) _previousCategoryIndex = availableWeapons.Count - 1;

                    var _previousCategory = (WeaponCategory)_previousCategoryIndex;
                    CycleCategortyLast(availableWeapons[_previousCategory]);
                }
                // Switch To Previous weapon
                else
                {
                    CycleCategoryPrevious(availableWeapons[currentWeapon.category]);                    
                }
                UpdateScrollPreviewIcons();
                UpdateSelectedIcon();
                UpdateAnimationController();
            }
            if (!isScrolling)
            {
                StartCoroutine(ShowScrollPreview());
                isScrolling = true;
            }
            timer = 0f;
        }
        else if (isScrolling)
        {
            timer += Time.deltaTime;
            if (timer >= scrollPreviewDuration)
            {
                isScrolling = false;
            }
        }
    }
    private void CheckAlphanumaricSwitch()
    {
        WeaponCategory _categoryToSelect = new WeaponCategory();
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            _categoryToSelect = WeaponCategory.Unarmed;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            _categoryToSelect = WeaponCategory.Melee;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            _categoryToSelect = WeaponCategory.Pistol;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            _categoryToSelect = WeaponCategory.Rifle;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            _categoryToSelect = WeaponCategory.Shotgun;
        }
        else
            return;
        SelectCategory(_categoryToSelect);
    }

    private void SelectCategory(WeaponCategory _category) {        
        if (availableWeapons.ContainsKey(_category)) {                        
            var weaponsOfType = availableWeapons[_category];
            if (currentWeapon == null) {
                CycleCategoryStart(weaponsOfType);
            }
            else if (currentWeapon.category != _category) {
                CycleCategoryStart(weaponsOfType);
            }
            else {
                CycleCategoryNext(weaponsOfType);
            }
            UpdateSelectedIcon();
            UpdateAnimationController();
        }
    }    
    private int GetIndexInCategory()
    {        
        for (int i = 0; i < availableWeapons[currentWeapon.category].Count; ++i)
            if (availableWeapons[currentWeapon.category][i] == currentWeapon)
                return i;
        return -1;
    }
    private void CycleCategoryNext(List<WeaponIdentifier> _weaponCategory)
    {
        if (currentWeapon != null)
        {
            currentWeapon.gameObject.name = "Unselected_" + currentWeapon.weaponName;
            currentWeapon.gameObject.SetActive(false);
        }
        for (int i = 0; i < _weaponCategory.Count; ++i)
        {
            if (_weaponCategory[i] == currentWeapon)
            {
                if (i == _weaponCategory.Count - 1)
                {
                    currentWeapon = _weaponCategory[0];

                }
                else currentWeapon = _weaponCategory[i + 1];
                currentWeapon.gameObject.name = currentWeapon.weaponName;
                currentWeapon.gameObject.SetActive(true);
                return;
            }
        }
    }
    private void CycleCategoryPrevious(List<WeaponIdentifier> _weaponCategory)
    {
        if (currentWeapon != null)
        {
            currentWeapon.gameObject.name = "Unselected_" + currentWeapon.weaponName;
            currentWeapon.gameObject.SetActive(false);
        }
        for (int i = 0; i < _weaponCategory.Count; ++i)
        {
            if (_weaponCategory[i] == currentWeapon)
            {
                if (i == 0)
                {
                    currentWeapon = _weaponCategory[_weaponCategory.Count - 1];

                }
                else currentWeapon = _weaponCategory[i - 1];
                currentWeapon.gameObject.name = currentWeapon.weaponName;
                currentWeapon.gameObject.SetActive(true);
                return;
            }
        }
    }
    public void CycleCategoryStart(List<WeaponIdentifier> _weaponCategory)
    {
        if (currentWeapon != null)
        {
            currentWeapon.gameObject.name = "Unselected_" + currentWeapon.weaponName;
            currentWeapon.gameObject.SetActive(false);
        }
        if (_weaponCategory.Count == 0)
        {
            currentWeapon = null;
            return;
        }

        currentWeapon = _weaponCategory[0];        
        currentWeapon.gameObject.name = currentWeapon.weaponName;
        currentWeapon.gameObject.SetActive(true);
    }
    public void CycleCategortyLast(List<WeaponIdentifier> _weaponCategory)
    {
        if (currentWeapon != null)
        {
            currentWeapon.gameObject.name = "Unselected_" + currentWeapon.weaponName;
            currentWeapon.gameObject.SetActive(false);
        }
        if (_weaponCategory.Count == 0)
        {
            currentWeapon = null;
            return;
        }
        currentWeapon = _weaponCategory[_weaponCategory.Count - 1];
        currentWeapon.gameObject.name = currentWeapon.weaponName;
        currentWeapon.gameObject.SetActive(true);
    }
    private void UpdateAnimationController()
    {
        if(currentWeapon == null)
        {
            PlayerAnimationController.current.SelectWeaponLayer(WeaponAnimationLayer.Unarmed);
        }
        else
        {
            PlayerAnimationController.current.SelectWeaponLayer(currentWeapon.animationLayer);
        }
    }
    #endregion

    #region icon refresh mehods
    WeaponIdentifier GetNextWeapon()
    {
        if(currentWeapon == null)
        {
            return availableWeapons[(WeaponCategory)1][0];
        }
        var _currentWeaponCategoryIndex = (int)currentWeapon.category;
        var _currentWeaponCategoryCount = availableWeapons[currentWeapon.category].Count - 1;
        var _currentWeaponIndexInList = GetIndexInCategory();

        if (_currentWeaponIndexInList == _currentWeaponCategoryCount)
        {
            if (_currentWeaponCategoryIndex == (availableWeapons.Count - 1))
            {
                return null;
            }
            var _nextList = availableWeapons[(WeaponCategory)_currentWeaponCategoryIndex + 1];            
            return _nextList[0];
        }
        return availableWeapons[(WeaponCategory)_currentWeaponCategoryIndex][_currentWeaponIndexInList + 1];
    }
    WeaponIdentifier GetPreviousWeapon()
    {
        if (currentWeapon == null)
            return availableWeapons[(WeaponCategory)availableWeapons.Count - 1][availableWeapons[(WeaponCategory)availableWeapons.Count - 1].Count - 1];
        var _currentWeaponTypeIndex = (int)currentWeapon.category;
        var _currentWeaponTypeList = availableWeapons[(WeaponCategory)_currentWeaponTypeIndex];
        var _currentWeaponIndexInList = GetIndexInCategory();
        if (_currentWeaponIndexInList == 0)
        {
            if (_currentWeaponTypeIndex == 0)
            {
                var _lastList = availableWeapons[(WeaponCategory)availableWeapons.Count - 1];
                return _lastList[_lastList.Count - 1];
            }
            var _previousList = availableWeapons[((WeaponCategory)_currentWeaponTypeIndex - 1)];
            if (_previousList.Count == 0)
                return null;
            var _lastOfPreviouseList = _previousList[_previousList.Count - 1];
            return _lastOfPreviouseList;
        }

        return _currentWeaponTypeList[_currentWeaponIndexInList - 1];
    }

    private void UpdateScrollPreviewIcons()
    {
        if (currentWeapon != null)
        {
            if (availableIcons.ContainsKey(currentWeapon.weaponName))
            {
                ShowSelectedIcon(currentWeapon);
            }
            else
            {
                CreateNewIcon(currentWeapon);
                ShowSelectedIcon(currentWeapon);
            }
        }
        else if (selectedWeaponIcon != null)
            selectedWeaponIcon.gameObject.SetActive(false);

        var _nextWeapon = GetNextWeapon();
        var _previousWeapon = GetPreviousWeapon();

        if( _nextWeapon == null)
        {
            ShowNextWeaponPreview(null);
        }
        else if (availableIcons.ContainsKey(_nextWeapon.weaponName))
        {
            ShowNextWeaponPreview(_nextWeapon);
        }
        else
        {
            CreateNewIcon(_nextWeapon);
            ShowNextWeaponPreview(_nextWeapon);
        }

        if(_previousWeapon == null)
        {
            ShowPreviousWeaponPreview(null);
        }
        else if (availableIcons.ContainsKey(_previousWeapon.weaponName))
        {
            ShowPreviousWeaponPreview(_previousWeapon);
        }
        else
        {
            CreateNewIcon(_previousWeapon);
            ShowPreviousWeaponPreview(_previousWeapon);
        }
    }
    private void UpdateSelectedIcon()
    {
        if(currentWeapon == null)
        {            
            return;
        }
        if (availableIcons.ContainsKey(currentWeapon.weaponName))
        {
            ShowSelectedIcon(currentWeapon);
        }
        else
        {
            CreateNewIcon(currentWeapon);
            ShowSelectedIcon(currentWeapon);
        }
        selectedWeaponIcon.gameObject.SetActive(true);
    }
    private void CreateNewIcon(WeaponIdentifier _iconToCreate)
    {
        GameObject newIcon = Instantiate(_iconToCreate.iconPrefab);
        newIcon.name = _iconToCreate.weaponName;
        newIcon.transform.SetParent(selectedWeaponIconHolder);
        newIcon.transform.localPosition = Vector3.zero;
        availableIcons.Add(_iconToCreate.weaponName, (RectTransform)newIcon.transform);
    }

    private void ShowSelectedIcon(WeaponIdentifier _iconToShow)
    {
        if (selectedWeaponIcon != null)
            selectedWeaponIcon.gameObject.SetActive(false);
        selectedWeaponIcon = availableIcons[_iconToShow.weaponName];
        selectedWeaponIcon.SetParent((Transform)selectedWeaponIconHolder);
        selectedWeaponIcon.localPosition = Vector3.zero;
        selectedWeaponIcon.gameObject.SetActive(true);
    }

    private void ShowNextWeaponPreview(WeaponIdentifier _iconToShow)
    {
        if (_iconToShow == null)
        {
            if (nextWeaponPreviewIcon != null)
                nextWeaponPreviewIcon.gameObject.SetActive(false);
            return;
        }
        if (nextWeaponPreviewIcon != null && nextWeaponPreviewIcon != currentWeapon)
            nextWeaponPreviewIcon.gameObject.SetActive(false);
        nextWeaponPreviewIcon = availableIcons[_iconToShow.weaponName];
        nextWeaponPreviewIcon.SetParent((Transform)nextWeaponPreviewIconHolder);
        nextWeaponPreviewIcon.localPosition = Vector3.zero;
        nextWeaponPreviewIcon.gameObject.SetActive(true);
    }
    private void ShowPreviousWeaponPreview(WeaponIdentifier _iconToShow)
    {
        if (_iconToShow == null)
        {
            if (previousWeaponPreviewIcon != null)
                previousWeaponPreviewIcon.gameObject.SetActive(false);
            return;
        }
        if (previousWeaponPreviewIcon != null && previousWeaponPreviewIcon != currentWeapon)
            previousWeaponPreviewIcon.gameObject.SetActive(false);
        previousWeaponPreviewIcon = availableIcons[_iconToShow.weaponName];
        previousWeaponPreviewIcon.SetParent((Transform)previousWeaponPreviewIconHolder);
        previousWeaponPreviewIcon.localPosition = Vector3.zero;
        previousWeaponPreviewIcon.gameObject.SetActive(true);
    }

    #endregion
}
