﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public enum WeaponAnimationLayer
{
    Unarmed,
    Melee_sawbat,
    Pistol_1911,
    Rifle_AK,
    Shotgun_sawedoff,
}

[System.Serializable]
public enum WeaponCategory
{
    Unarmed,
    Melee,
    Pistol,
    Rifle,
    Shotgun
}


[RequireComponent(typeof(WeaponBehaviour))]
public class WeaponIdentifier : MonoBehaviour
{
    public string weaponName;
    [TextArea] public string description;
    public WeaponCategory category;
    public WeaponAnimationLayer animationLayer;
    public GameObject iconPrefab;
    public WeaponBehaviour behaviour;

    private void OnEnable()
    {
        if(behaviour == null)
            behaviour = GetComponent<WeaponBehaviour>();
    }
    public static List<WeaponAnimationLayer> availableLayers()
    {
        var _newList = new List<WeaponAnimationLayer>();
        _newList.Add(WeaponAnimationLayer.Unarmed);
        _newList.Add(WeaponAnimationLayer.Melee_sawbat);
        _newList.Add(WeaponAnimationLayer.Pistol_1911);
        _newList.Add(WeaponAnimationLayer.Rifle_AK);
        _newList.Add(WeaponAnimationLayer.Shotgun_sawedoff);
        return _newList;
    }

}
public static class AnimationLayerControll
{
    public static int LayerTranformIndex(this WeaponAnimationLayer _layer)
    {
        return (int)_layer - 1;
    }

    public static List<string> availableAnimationLayers()
    {        
        var _newList = new List<string>();
        _newList.Add(WeaponAnimationLayer.Unarmed.ToString());
        _newList.Add(WeaponAnimationLayer.Melee_sawbat.ToString());
        _newList.Add(WeaponAnimationLayer.Pistol_1911.ToString());
        _newList.Add(WeaponAnimationLayer.Rifle_AK.ToString());
        _newList.Add(WeaponAnimationLayer.Shotgun_sawedoff.ToString());
        return _newList;
    }    
}

