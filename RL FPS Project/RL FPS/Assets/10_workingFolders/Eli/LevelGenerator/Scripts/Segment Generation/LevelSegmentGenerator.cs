﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class LevelSegmentGenerator : MonoBehaviour {

    public enum SegmentLayout { Default, Corridor, PremadeRooms }
    public SegmentLayout layout;
    public bool attemptToMaximiseRoomCount;
    public int premadeRoomsToCreate;
    public Vector2Int RandomCoordinates {
        get {
            return new Vector2Int(Random.Range(0, LevelGenerator.current.segmentSize.x), Random.Range(0, LevelGenerator.current.segmentSize.y));
        }
    }
    [Header("Refrences")]
    public SegmentCell cellPrefab;    
    public CellEdge passagePrefab;
    public CellDoor doorPrefab;
    public CellWall[] wallPrefabs;
    public LevelSegment parentSegment;
    public GameObject cornerPrefab, exitZonePrefab;
    public PremadeRoom[] premadeRoomsPrefabs;
    public SegmentRoomSettings[] roomSettings;
    public List<SegmentRoom> rooms { get; private set; } = new List<SegmentRoom>();
    public bool done;
    [HideInInspector] public List<PremadeRoom> premadeRooms = new List<PremadeRoom>();
    public SegmentCell[][] cells;

    public void GenerateWholeSegment(LevelSegment _parent)
    {
        done = false;
        parentSegment = _parent;
        if(layout == SegmentLayout.Default)
        {
            CreateDefaultLayout();
            RenameRooms();
            EnsureAccesToAllRooms();
            CreateCorners();
        }
        else if (layout == SegmentLayout.Corridor)
        {
            CreateCorridorLayout();
            RenameRooms();
            CreateCorners();
        }
        else if (layout == SegmentLayout.PremadeRooms)
        {
            CreatePremadeLayout();            
            RenameRooms();            
            CreateCorners();
        }
        done = true;
    }        
    public void CreatePremadeLayout()
    {
        InitializeCellArray();
        List<SegmentCell> activeCells = new List<SegmentCell>();        
        CreateSegmentPath(activeCells);

        var unoccupiedCells = GetUnoccupiedCells();
        if(parentSegment.GetEntrance().open)
        {
            if(parentSegment.GetExit().open)
            {
                // create a mid-level segment
                var canAddRooms = true; // dont chande to 'true' until method is ready for testing;        
                var roomsCreated = 0;                
                while (canAddRooms)
                {
                    if (unoccupiedCells.Count == 0) {
                        canAddRooms = false;
                        continue;
                    }

                    var roomToTry = premadeRoomsPrefabs[Random.Range(0, premadeRoomsPrefabs.Length)];
                    var randomCellToCheck = unoccupiedCells[Random.Range(0, unoccupiedCells.Count)];                                            
                    var newRoom = GetNewRoomInfo(roomToTry, randomCellToCheck, activeCells);

                    // create room at position randomCellToCheck with rotation randomPossibleDir                                   
                    if (newRoom.connected)
                    {
                        // create new premade room                        
                        var _keys = new List<MovementDirections>();
                        foreach(MovementDirections _dir in newRoom.poeInfo.Keys)
                        {
                            _keys.Add(_dir);
                        }
                        var randomPossibleDir = _keys[Random.Range(0, newRoom.poeInfo.Keys.Count)];                        
                        var newPremadeRoom = CreatePremadeRoom(roomToTry, randomCellToCheck ,randomPossibleDir);
                        // get updated unoccupied cell list
                        unoccupiedCells = GetUnoccupiedCells();

                        // create path from each new room entrance to an existing segment path                        
                        foreach(PointOfEntryInfo _poe in newRoom.poeInfo[randomPossibleDir])
                        {
                            //List<PathStepInfo> newRoomPath = FindPathForNewRoom(activeCells, randomCellToCheck, randomPossibleDir, _poe);  
                            var newRoomPath = _poe.createdPath;                                                        
                            foreach(MovementDirections _dir in MazeDirections.AllDirections())
                            {
                                if (!ContainsCoordinates(newRoomPath[0].coords + _dir.ToIntVector2()))
                                    continue;
                                if (GetCell(newRoomPath[0].coords + _dir.ToIntVector2()) == newPremadeRoom)
                                    CreateNewRoomPath(activeCells, newRoomPath, _dir);
                            }
                        }
                        premadeRooms.Add(newPremadeRoom);
                                         
                        roomsCreated++;
                        if(roomsCreated > premadeRoomsToCreate)
                        {
                            canAddRooms = false;
                        }
                    }
                    else
                    {
                        unoccupiedCells.Remove(randomCellToCheck);
                        if(unoccupiedCells.Count == 0)
                        {
                            canAddRooms = false;
                        }
                        continue;
                    }
                }                
            }
            else
            {                
                // create last segment in level, include an exit point
            }
        }
        else
        {
            if(parentSegment.GetExit().open)
            {                
                // create first segment in level, include player spawn point
            }
            else
            {
                // create an isolated segment, unused
            }
        }                        
        AddWalls(activeCells);             
    }

    private List<PathStepInfo> FindPathForNewRoom(List<SegmentCell> activeCells, Vector2Int roomOrigin, MovementDirections roomRotation, PointOfEntry _poe, Vector2Int roomSize)
    {        
        List<PathStepInfo> newRoomPath = new List<PathStepInfo>();
        List<Vector2Int> visitedCells = new List<Vector2Int>();
        List<Vector2Int> _occupiedCells = new List<Vector2Int>();        
        for (int x = 0; x < roomSize.x; ++x)
        {
            for (int y = 0; y < roomSize.y; ++y)
            {
                _occupiedCells.Add(roomOrigin + roomRotation.ToIntVector2() * y + roomRotation.GetNextClockwise().ToIntVector2() * x);
            }
        }

        var _poePathStartPosition = roomOrigin + roomRotation.AlignGrid(_poe.address) + _poe.wall.RelativeTo(roomRotation).ToIntVector2();
        var _pathfindingCurrentDir = _poe.wall.RelativeTo(roomRotation);
        if (!ContainsCoordinates(_poePathStartPosition))
            return new List<PathStepInfo>();
        if (GetCell(_poePathStartPosition) is PremadeRoom)
            return new List<PathStepInfo>();
        var _firstPathCellInfo = AddPathStep(_pathfindingCurrentDir, _poePathStartPosition);
        visitedCells.Add(_firstPathCellInfo.coords);
        newRoomPath.Add(_firstPathCellInfo);

        var pathCreated = activeCells.Contains(GetCell(_firstPathCellInfo.coords)); // Change To True to disable path finding
        while (!pathCreated && newRoomPath.Count != 0)
        {
            // select the last cell in the current path list 
            var currentCellChecked = newRoomPath[newRoomPath.Count - 1];            
            #region Check All direction and find valid direction, or complete path
            // check whether the pathfinder has reached an existing path (complete), and get updated valid progression directions
            List<MovementDirections> _remainingAvailabledirs = new List<MovementDirections>();
            _remainingAvailabledirs.AddRange(MazeDirections.AllDirections());
            foreach (MovementDirections _dir in MazeDirections.AllDirections())
            {
                var _potentialNextPathStep = currentCellChecked.coords + _dir.ToIntVector2();
                if (ContainsCoordinates(_potentialNextPathStep))
                {
                    bool _connectionFound = activeCells.Contains(GetCell(_potentialNextPathStep)); // is SegmentCell); ; ;// && !(GetCell(_potentialNextPathStep) is PremadeRoom);
                    bool _visited = visitedCells.Contains(_potentialNextPathStep);
                    bool _occupied = (GetCell(_potentialNextPathStep) is PremadeRoom) || _occupiedCells.Contains(_potentialNextPathStep);
                    bool _valid = !_occupied && !_visited;
                    if (_connectionFound)
                    {
                        pathCreated = true;
                    }
                    else if (!_valid)
                    {
                        _remainingAvailabledirs.Remove(_dir);
                    }
                }
                else
                {
                    _remainingAvailabledirs.Remove(_dir);
                }
            }
            #endregion
            #region Check if complete or cant continue
            if (currentCellChecked.coords == newRoomPath[0].coords && _remainingAvailabledirs.Count == 0)
            {
                pathCreated = true;
                return new List<PathStepInfo>();                
            }
            if (pathCreated)
                continue;
            #endregion

            // update valid directions
            currentCellChecked.availableDirs = _remainingAvailabledirs;

            #region Find next step
            // jump a step back if no valid directions to test remain;
            if (currentCellChecked.availableDirs.Count == 0)
            {
                visitedCells.Add(currentCellChecked.coords);
                StepBack(newRoomPath);
                continue;
            }

            // check if an existing path is in line of sight
            var pathInLOS = false;

            var turn = Random.Range(0, 5) == 0 ? true : false;
            // change the direction of the path if the current path direction is unavailable, or a turn is rolled
            if (!currentCellChecked.availableDirs.Contains(_pathfindingCurrentDir) || turn)
            {
                // if there is only one direction to move, select it
                if (currentCellChecked.availableDirs.Count == 1)
                {
                    _pathfindingCurrentDir = currentCellChecked.availableDirs[0];
                }
                // if can't turn right, turn left
                else if (!currentCellChecked.availableDirs.Contains(_pathfindingCurrentDir.GetNextClockwise()) && currentCellChecked.availableDirs.Contains(_pathfindingCurrentDir.GetNextCounterclockwise()))
                {
                    _pathfindingCurrentDir = _pathfindingCurrentDir.GetNextCounterclockwise();
                }
                // if can't turn left, turn right
                else if (currentCellChecked.availableDirs.Contains(_pathfindingCurrentDir.GetNextClockwise()) && !currentCellChecked.availableDirs.Contains(_pathfindingCurrentDir.GetNextCounterclockwise()))
                {
                    _pathfindingCurrentDir = _pathfindingCurrentDir.GetNextClockwise();
                }
                // if can turn both left and right, pick a random side
                else if (currentCellChecked.availableDirs.Contains(_pathfindingCurrentDir.GetNextClockwise()) && currentCellChecked.availableDirs.Contains(_pathfindingCurrentDir.GetNextCounterclockwise()))
                {
                    _pathfindingCurrentDir = Random.Range(0, 2) != 0 ? _pathfindingCurrentDir.GetNextClockwise() : _pathfindingCurrentDir.GetNextCounterclockwise();
                }
            }
            #endregion
            #region Take next step
            // add path cell in selected direction
            PathStepInfo _nextPathStep = AddPathStep(_pathfindingCurrentDir, currentCellChecked.coords + _pathfindingCurrentDir.ToIntVector2());
            visitedCells.Add(_nextPathStep.coords);
            newRoomPath.Add(_nextPathStep);
            #endregion
        }

        return newRoomPath;
    }

    private void CreateNewRoomPath(List<SegmentCell> activeCells, List<PathStepInfo> newRoomPath, MovementDirections _entranceDir)
    {
        List<Vector2Int> createdPathCells = new List<Vector2Int>();
        for(int i = 0; i < newRoomPath.Count; ++i) {
            if (activeCells.Contains(GetCell(newRoomPath[i].coords)))
                if (i == 0) { 
                    CreateDoor(GetCell(newRoomPath[i].coords), _entranceDir);
                    continue;
                }           
            createdPathCells.Add(newRoomPath[i].coords);
            SegmentCell newPathCell = CreateCell(newRoomPath[i].coords);
            newPathCell.Initialize(CreateRoomByIndex(1));
            if (i == 0)
                CreateDoor(newPathCell, _entranceDir);
            activeCells.Add(newPathCell);
        }
    }

    private static void StepBack(List<PathStepInfo> newRoomPath)
    {
        var currentPathIndex = newRoomPath.Count - 1;
        newRoomPath.RemoveAt(currentPathIndex);        
    }
    private static PathStepInfo AddPathStep(MovementDirections _pathfindingCurrentDir, Vector2Int stepCoords)
    {
        var _nextPathStep = new PathStepInfo();

        _nextPathStep.availableDirs = new List<MovementDirections>();        
        _nextPathStep.coords = stepCoords;
        _nextPathStep.availableDirs.AddRange(MazeDirections.AllDirections());
        _nextPathStep.availableDirs.Remove(_pathfindingCurrentDir.GetOpposite());        
        return _nextPathStep;
    }



    #region corridor layout creation methods
    void CreateCorridorLayout()
    {
        InitializeCellArray();
        List<SegmentCell> activeCells = new List<SegmentCell>();
        var newPath = CreateSegmentPath(activeCells);        
        AddWalls(newPath);
    }

    private void AddWalls(List<SegmentCell> newPath)
    {
        foreach (SegmentCell _cell in newPath)
        {
            var _edges = _cell.GetEdges();
            foreach (MovementDirections _dir in MazeDirections.AllDirections())
            {
                var neighborAddress = _cell.coordinates + _dir.ToIntVector2();
                if (_cell.GetEdge(_dir) != null)
                    continue;
                if (!ContainsCoordinates(neighborAddress))
                {
                    if (_cell.GetEdge(_dir) == null)
                    {
                        CreateWall(_cell, null, _dir);
                    }
                    continue;
                }
                if (GetCell(neighborAddress) != null && !(GetCell(neighborAddress) is PremadeRoom))
                {
                    CreatePassageInSameRoom(_cell, GetCell(neighborAddress), _dir);
                }
                else
                {
                    CreateWall(_cell, null, _dir);
                }
            }
        }
    }

    List<SegmentCell> CreateSegmentPath(List<SegmentCell> activeCells)
    {
        LevelSegment.OpeningParameters entrance = parentSegment.GetEntrance();
        LevelSegment.OpeningParameters exit = parentSegment.GetExit();
        List<SegmentCell> pathCells = new List<SegmentCell>();        
        if (entrance.open)
        {        
            // mid path segment
            if (exit.open)
            {
                if (entrance.openingAddress == exit.openingAddress)
                {
                    SegmentCell entranceCell = CreateCell(entrance.openingAddress);
                    entranceCell.Initialize(CreateRoomByIndex(1));
                    activeCells.Add(entranceCell);
                    pathCells.Add(entranceCell);
                    CreateDoor(entranceCell, entrance.dir);                    
                    CreateDoor(entranceCell, exit.dir);
                    return pathCells;
                }

                int dX = exit.openingAddress.x - entrance.openingAddress.x;
                int dY = exit.openingAddress.y - entrance.openingAddress.y;
                List<MovementDirections> path = new List<MovementDirections>();
                CalculateMainPath(entrance, dX, dY, path);
                for(int i = 0; i <= path.Count; ++i)
                {
                    // create first segment path cell at entrance address
                    if(i == 0)
                    {
                        SegmentCell entranceCell = CreateCell(entrance.openingAddress);
                        entranceCell.Initialize(CreateRoomByIndex(1));
                        activeCells.Add(entranceCell);
                        pathCells.Add(entranceCell);
                        CreateDoor(entranceCell, entrance.dir);
                    }
                    // create last segment path cell                  
                    else if(i == path.Count)
                    {                        
                        SegmentCell exitCell = CreateCell(exit.openingAddress);
                        exitCell.Initialize(CreateRoomByIndex(1));
                        pathCells.Add(exitCell);
                        activeCells.Add(exitCell);
                        CreateDoor(exitCell, exit.dir);
                    }
                    // create mid segment path cell
                    else
                    {
                        Vector2Int newCellDirection = pathCells[i - 1].coordinates + path[i - 1].ToIntVector2();
                        SegmentCell newPathCell = CreateCell(newCellDirection);
                        newPathCell.Initialize(CreateRoomByIndex(1));
                        activeCells.Add(newPathCell);
                        pathCells.Add(newPathCell);
                    }
                }
                // generate Segment
            }
            // path end segment
            else
            {
                SegmentCell entranceCell = CreateCell(entrance.openingAddress);
                entranceCell.Initialize(CreateRoomByIndex(1));
                activeCells.Add(entranceCell);
                CreateDoor(entranceCell, entrance.dir);
                // generate Last Segment
            }
        }
        // path start segment
        else
        {
            if (exit.open)
            {
                SegmentCell exitCell = CreateCell(exit.openingAddress);
                exitCell.Initialize(CreateRoomByIndex(1));                
                activeCells.Add(exitCell);
                CreateDoor(exitCell, exit.dir);                
            }
            // invalid\isolated segment
            else
            {
                Debug.LogWarning("Cant Generate Path, isolated Segment!");
            }
        }

        return pathCells;
    }
    private static void CalculateMainPath(LevelSegment.OpeningParameters entrance, int dX, int dY, List<MovementDirections> path)
    {
        if (entrance.dir == MovementDirections.Left || entrance.dir == MovementDirections.Right)
        {
            for (int i = 0; i < Mathf.Abs(dX); ++i)
            {
                if (dX < 0)
                {
                    path.Add(MovementDirections.Left);
                }
                else
                {
                    path.Add(MovementDirections.Right);
                }
            }
            for (int i = 0; i < Mathf.Abs(dY); ++i)
            {
                if (dY < 0)
                {
                    path.Add(MovementDirections.Back);
                }
                else
                {
                    path.Add(MovementDirections.Front);
                }
            }
        }
        else
        {
            for (int i = 0; i < Mathf.Abs(dY); ++i)
            {
                if (dY < 0)
                {
                    path.Add(MovementDirections.Back);
                }
                else
                {
                    path.Add(MovementDirections.Front);
                }
            }
            for (int i = 0; i < Mathf.Abs(dX); ++i)
            {
                if (dX < 0)
                {
                    path.Add(MovementDirections.Left);
                }
                else
                {
                    path.Add(MovementDirections.Right);
                }
            }
        }
    }
    #endregion

    #region default layout creation methods
    private void CreateDefaultLayout()
    {
        InitializeCellArray();
        List<SegmentCell> activeCells = new List<SegmentCell>();
        CreateSegmentPath(activeCells);
        FillSegmentRandomly(activeCells);
    }
    private void FillSegmentRandomly(List<SegmentCell> activeCells)
    {
        DoFirstGenerationStep(activeCells);         // place the cell generator on a randon starting position
        while (activeCells.Count > 0)               // this loops runs as long as there are un-initialized cells in the active cells list
        {
            DoNextGenerationStep(activeCells);
        }
    }
    private void DoFirstGenerationStep(List<SegmentCell> activeCells) {
        // place the cell generator on a randon starting position
        SegmentCell newCell = CreateCell(RandomCoordinates);   // create the first cell in a random position
        newCell.Initialize(CreateRoom(-1));                 // initialize the first cell with a newly created room with any randon setting index
        activeCells.Add(newCell);                           // add first cell to list of ALL OCCUPIED cells        
        // if this is the first segment of the level, move player to the first cell created
        if(!parentSegment.GetEntrance().open)
        {
            if(LevelManager.current.player!= null) LevelManager.current.player.transform.position = newCell.transform.position + new Vector3(0f, 5f, 0f); 
        }
    }
    private void DoNextGenerationStep(List<SegmentCell> activeCells) {

        #region Select Last Cell In List
        int currentIndex = activeCells.Count - 1;
        SegmentCell currentCell = activeCells[currentIndex];                               // pick the last cell to be added to the list
        #endregion
        #region Check If Selected Cell Is Fully Initialized
        if (currentCell.IsFullyInitialized) {
            // if this cell is completly initialized (has 4 CellEgde refrences in the tracking list), remove it from active cells list and end this cycle
            activeCells.RemoveAt(currentIndex);
            if(activeCells.Count == 0 )
            {
                if(!parentSegment.GetExit().open)
                {
                    GameObject newZone =  Instantiate(exitZonePrefab, transform);
                    newZone.tag = "Exit Zone";
                }                
            }
            return;
        }
        #endregion
        #region Pick A Random UnInitialized Direction
        MovementDirections direction = currentCell.RandomUninitializedDirection;        // create a new local direction enum which contain a random direction from the cell
        Vector2Int neighbhorCoordinates = currentCell.coordinates + direction.ToIntVector2();    // tranform picked direction into vector2 coordinated
        #endregion
        #region Initialize Selected Direction       
        if (ContainsCoordinates(neighbhorCoordinates)) {
            // run only IF the direction is within the bounds of the Maze
            #region Create a new Neigbhor and Initialize Appropriate Direction
            SegmentCell neighbor = GetCell(neighbhorCoordinates);                                   // get the information about the cell at the destination coordinates

            if (neighbor == null) {
                // if no cell created at the selected direction coordibates yet
                neighbor = CreateCell(neighbhorCoordinates);                                     // create cell
                CreatePassage(currentCell, neighbor, direction);                        // create a passage between the two cells, and determine if neigbhor is part of the same room
                activeCells.Add(neighbor);                                              // add neigbhor to active Cells qeuee;
            }
            else if (currentCell.room.settingsIndex == neighbor.room.settingsIndex) {
                // else if the other cell is in the same room as this cell
                CreatePassageInSameRoom(currentCell, neighbor, direction);              // create an internal passage                                
            }
            else {
                // otherwise, create a wall
                CreateWall(currentCell, neighbor, direction);                
            }
            #endregion
        }        
        else {
            // if at edge of segment, create a wall at the edge
            CreateWall(currentCell, null, direction);                                 
        }
        #endregion
    }
    #endregion

    #region  cell manipulation methods
    private SegmentCell CreateCell (Vector2Int coordinates) {        
		SegmentCell newCell = Instantiate(cellPrefab) as SegmentCell;                 // create cell prefab (uniform)
		cells[coordinates.x][ coordinates.y] = newCell;                          // add cell to a list of ALL cells		
		newCell.name = "Maze Cell " + coordinates.x + ", " + coordinates.y;     // define cell name
        newCell.coordinates = coordinates;                                      // define cell position <<--- IMPORTANT
        newCell.transform.parent = transform;
        newCell.generator = this;
        float cellSize = LevelGenerator.current.cellSizeFactor;
		newCell.transform.localPosition = new Vector3((coordinates.x - LevelGenerator.current.segmentSize.x * 0.5f + 0.5f)*cellSize, 0f, (coordinates.y - LevelGenerator.current.segmentSize.y * 0.5f + 0.5f) * cellSize);
        newCell.transform.localScale = Vector3.one * cellSize;
		return newCell;
	}
    private void InitializeCellArray()
    {
        cells = new SegmentCell[LevelGenerator.current.segmentSize.x][];
        for (int i = 0; i < cells.Length; ++i)
        {
            cells[i] = new SegmentCell[LevelGenerator.current.segmentSize.y];
        }
    }
    public bool ContainsCoordinates(Vector2Int coordinate) {
        return coordinate.x >= 0 && coordinate.x < LevelGenerator.current.segmentSize.x && coordinate.y >= 0 && coordinate.y < LevelGenerator.current.segmentSize.y;
    }
    public SegmentCell[][] GetCells() 
    { 
        return cells; 
    }
    private List<Vector2Int> GetUnoccupiedCells()
    {
        var unoccupiedCells = new List<Vector2Int>();
        for (int x = 0; x < cells.Length; ++x) {
            for (int y = 0; y < cells[x].Length; ++y) {
                if (cells[x][y] == null)
                    unoccupiedCells.Add(new Vector2Int(x, y));
            }
        }
        return unoccupiedCells;
    }
    private List<Vector2Int> GetOccupiedCells()
    {
        var occupiedCells = new List<Vector2Int>();
        for (int x = 0; x < cells.Length; ++x) {
            for (int y = 0; y < cells[x].Length; ++y) {
                if (cells[x][y] != null)                    
                    occupiedCells.Add(new Vector2Int(x, y));
            }
        }
        return occupiedCells;
    }
    private Vector2Int GetRandomCellAddressFromList(List<Vector2Int> listToPickFrom)
    {
        var randomIndex = Random.Range(0, listToPickFrom.Count);
        return listToPickFrom[randomIndex];
    }
    public SegmentCell GetCell(Vector2Int coordinates)
    { 
        return cells[coordinates.x][coordinates.y];
    }    
    #endregion

    #region utility methods
    private void EnsureAccesToAllRooms()
    {
        for (int i = 0; i < rooms.Count; ++i)
        {
            rooms[i].AccessCheck();
        }
    }
    #endregion

    #region edge creation methods
    private void CreateWall (SegmentCell cell, SegmentCell otherCell, MovementDirections direction) {
		CellWall wall = Instantiate(wallPrefabs[Random.Range(0, wallPrefabs.Length)]) as CellWall;
		wall.Initialize(cell, otherCell, direction);
		if (otherCell != null) {
			wall = Instantiate(wallPrefabs[Random.Range(0, wallPrefabs.Length)]) as CellWall;
			wall.Initialize(otherCell, cell, direction.GetOpposite());
		}
	}
    public void CreateDoor (SegmentCell cell, MovementDirections direction) {
		CellDoor door = Instantiate(doorPrefab);
		door.Initialize(cell, null, direction);		
	}
	private void CreatePassage (SegmentCell cell, SegmentCell otherCell, MovementDirections direction) {        
		CellEdge prefab = Random.value < LevelGenerator.current.doorProbability ? doorPrefab : passagePrefab;    // pick between a door or a passage prefab
        CellEdge passage;
        if (prefab == doorPrefab)
        {
            passage = Instantiate(prefab) as CellEdge;
        }
        else
        {
		    passage = Instantiate(prefab) as CellPassage;                                                   // create selected passage, MazePassage is a sub-parent from which MazeDoor and MazeWall inherit
        }
		passage.Initialize(cell, otherCell, direction);                                                             // initialize passage
        
        if (prefab == doorPrefab)
        {
            passage = Instantiate(prefab) as CellEdge;
			otherCell.Initialize(CreateRoom(cell.room.settingsIndex));                                              // Create a new room on the other side of a door and initialze the other cell with it
        }
        else
        {
            passage = Instantiate(prefab) as CellPassage;                                                   // create selected passage, MazePassage is a sub-parent from which MazeDoor and MazeWall inherit
			otherCell.Initialize(cell.room);                                                                        // initialize the other cell with the same room as this 
        }        
        passage.Initialize(otherCell, cell, direction.GetOpposite());
		
	}
	private void CreatePassageInSameRoom (SegmentCell cell, SegmentCell otherCell, MovementDirections direction) 
    {
		CellPassage passage = Instantiate(passagePrefab) as CellPassage;        // instansiate a passage prefab
		passage.Initialize(cell, otherCell, direction);                         // initialize the prefab on current cell (leave empty)
		passage = Instantiate(passagePrefab) as CellPassage;                    // instansiate a passage prefab on the neigbhor in the oppisite direction
		passage.Initialize(otherCell, cell, direction.GetOpposite());           // initialize the oppisite side of the passage (live enpty as well)
		if (cell.room != otherCell.room) {
			SegmentRoom roomToAssimilate = otherCell.room;  
			cell.room.Assimilate(roomToAssimilate);
			rooms.Remove(roomToAssimilate);
			Destroy(roomToAssimilate);
		}
	}
    public void CreateAssuredPassage(SegmentCell cell, SegmentCell otherCell, MovementDirections direction)
    {
        CellDoor door = Instantiate(doorPrefab);
        CellDoor doorOther = Instantiate(doorPrefab);
        door.Initialize(cell, null, direction);
        doorOther.Initialize(otherCell, null, direction.GetOpposite());
    }
    private void CreateCorners()
    {
        var allCell = new List<SegmentCell>();
        foreach(SegmentCell[] cellArray in cells) {
            allCell.AddRange(cellArray);
        }
        foreach(SegmentCell cell in allCell)
        {
            if (cell == null)
                continue;
            if ((cell is PremadeRoom))
                continue;
            cell.CheckCorners();
        }        
    }
    #endregion

    #region Room manipulation methods
    private void RenameRooms()
    {
        for (int i = 0; i < rooms.Count; ++i)
        {
            rooms[i].RoomName = "Room " + i;
        }
    }
    private SegmentRoom CreateRoom (int indexToExclude) {
		SegmentRoom newRoom = ScriptableObject.CreateInstance<SegmentRoom>();     // create new scriptable object (hold information about a room)
		newRoom.settingsIndex = Random.Range(0, roomSettings.Length);       // setting index controls the type of room to create from an array of posiible presets
		if (newRoom.settingsIndex == indexToExclude)                        // need to see what this does 
        {
			newRoom.settingsIndex = (newRoom.settingsIndex + 1) % roomSettings.Length;
		}
		newRoom.settings = roomSettings[newRoom.settingsIndex];             // define new room type
		rooms.Add(newRoom);                                                 // add new room to list of all rooms
		return newRoom;
	}
    private SegmentRoom CreateRoomByIndex(int indexToUse)
    {
        SegmentRoom newRoom = ScriptableObject.CreateInstance<SegmentRoom>();     // create new scriptable object (hold information about a room)
        newRoom.settingsIndex = indexToUse;       // setting index controls the type of room to create from an array of posiible presets        
        newRoom.settings = roomSettings[newRoom.settingsIndex];             // define new room type
        newRoom.RoomName = "Main_room_" + parentSegment.name;
        rooms.Add(newRoom);                                                 // add new room to list of all rooms
        return newRoom;
    }
    #endregion

    #region Premade Rooms Manipulation methods
    private PremadeRoom CreatePremadeRoom(PremadeRoom roomPrefab, Vector2Int coordinates, MovementDirections dir)
    {
        PremadeRoom newPremadeRoom = Instantiate(roomPrefab);
        newPremadeRoom.name = "Room " + coordinates.x + ", " + coordinates.y;
        newPremadeRoom.coordinates = coordinates;
        newPremadeRoom.transform.parent = transform;
        newPremadeRoom.generator = this;
        newPremadeRoom.Initialize(CreateRoomByIndex(0));
        float cellSize = LevelGenerator.current.cellSizeFactor;
        newPremadeRoom.transform.localPosition = new Vector3((coordinates.x - LevelGenerator.current.segmentSize.x * 0.5f + 0.5f) * cellSize, 0f, (coordinates.y - LevelGenerator.current.segmentSize.y * 0.5f + 0.5f) * cellSize);
        newPremadeRoom.transform.localScale = Vector3.one * cellSize;
        newPremadeRoom.transform.localRotation = dir.ToRotation();
        for(int x = 0; x < roomPrefab.roomSize.x; ++x)
            for(int y = 0; y < roomPrefab.roomSize.y; ++y) {
                var address = coordinates + dir.GetNextClockwise().ToIntVector2() * x + dir.ToIntVector2() * y;                
                cells[address.x][address.y] = newPremadeRoom;
            }                   
        return newPremadeRoom;
    }
    private RoomPathConnectionInfo GetNewRoomInfo(PremadeRoom roomToTry, Vector2Int originCell, List<SegmentCell> activePathCells)
    {
        // a method to return information about possible path connection and orientaion for a given room
        var connected = false;
        RoomPathConnectionInfo newInfo = new RoomPathConnectionInfo();
        newInfo.poeInfo = new Dictionary<MovementDirections, List<PointOfEntryInfo>>();        
        // try to fit the room into the specified cell at possible rotation
        List<MovementDirections> possibleRoomRotations = TryToFit(roomToTry, originCell);

        // Check each possible room rotaiton (_dirToCheck)
        foreach (MovementDirections _dirToCheck in possibleRoomRotations)
        {
            // List of points of entry, that have a possible path connection
            Dictionary<PointOfEntry, List<PathStepInfo>> unblockedPOEs = new Dictionary<PointOfEntry, List<PathStepInfo>>();
            // Create an updated list of occupied cells that contains cells that will be occupied by the current room
            List<Vector2Int> _occupiedCells = new List<Vector2Int>();
            foreach(SegmentCell _occupiedCell in activePathCells)
            {
                _occupiedCells.Add(_occupiedCell.coordinates);
            }
            for (int x = 0; x < roomToTry.roomSize.x; ++x)
            {
                for (int y = 0; y < roomToTry.roomSize.y; ++y)
                {
                    _occupiedCells.Add(originCell + _dirToCheck.ToIntVector2() * y + _dirToCheck.GetNextClockwise().ToIntVector2() * x);
                }
            }

            // Check for possible path connection for each point of entry of the room (roomToTry) in the rotaiton (_dirToCheck)
            foreach (PointOfEntry _poe in roomToTry.pointsOfEntry)
            {
                var newRoomPath = FindPathForNewRoom(activePathCells, originCell, _dirToCheck, _poe, roomToTry.roomSize);
                if(newRoomPath.Count == 0)
                {
                    continue;
                }                
                unblockedPOEs.Add(_poe, newRoomPath);                                                
            }
            
            if (unblockedPOEs.Count != 0) {
                connected = true;
                newInfo.poeInfo.Add(_dirToCheck, new List<PointOfEntryInfo>());                
                foreach(PointOfEntry unblockedPOE in unblockedPOEs.Keys)
                {
                    var newpoeInfo = new PointOfEntryInfo();
                    newpoeInfo.possibleRoomRotaion = _dirToCheck;
                    newpoeInfo.poe = unblockedPOE;
                    newpoeInfo.createdPath = unblockedPOEs[unblockedPOE];
                    newInfo.poeInfo[_dirToCheck].Add(newpoeInfo);
                }
            }
        }        
        newInfo.connected = connected;
        return newInfo;
    }    

    private List<MovementDirections> TryToFit(PremadeRoom roomToTry, Vector2Int originCell)
    {
        List<MovementDirections> possibleRoomRotations = new List<MovementDirections>();
        possibleRoomRotations.AddRange(MazeDirections.AllDirections());
        foreach (MovementDirections _dir in MazeDirections.AllDirections())
        {
            for (int x = 0; x < roomToTry.roomSize.x; ++x)
            {
                for (int y = 0; y < roomToTry.roomSize.y; ++y)
                {
                    if (ContainsCoordinates(originCell + _dir.ToIntVector2() * y + _dir.GetNextClockwise().ToIntVector2() * x))
                    {
                        if (GetCell(originCell + _dir.ToIntVector2() * y + _dir.GetNextClockwise().ToIntVector2() * x) != null)
                        {
                            possibleRoomRotations.Remove(_dir);
                        }
                    }
                    else
                        possibleRoomRotations.Remove(_dir);
                }
            }
        }

        return possibleRoomRotations;
    }
    
    #endregion
}