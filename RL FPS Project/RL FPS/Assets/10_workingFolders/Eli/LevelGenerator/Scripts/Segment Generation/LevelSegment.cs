﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSegment : MonoBehaviour
{    
    Dictionary<MovementDirections, OpeningParameters> openings = new Dictionary<MovementDirections,OpeningParameters>();        
    public LevelSegmentGenerator levelSegmentGenerator;
    public SegmentEnemyPopulator enemySpawner;
    OpeningParameters entrance, exit; 

    [ContextMenu("Set Up Room")]
    public void CreateFirstSegment()
    {        
        DefineDictionery();
    }
    
    public void CreateSegmentLayout(MovementDirections entranceDir, Vector2Int entranceAddress)
    {
        DefineDictionery();
                                
        entrance = openings[entranceDir];
        entrance.open = true;
        entrance.openingAddress = entranceAddress;
    }

    public void FinilizeSegmentLayout(MovementDirections exitDir, string _name)
    {
        gameObject.name = _name;
        exit = openings[exitDir];        
        exit.open = true;        
        if(entrance.open)
        {
            // if entrance is BACK
            if(entrance.dir == MovementDirections.Back)
            {            
                // turn LEFT
                if (exit.dir == MovementDirections.Left)
                {                
                    exit.openingAddress = new Vector2Int(
                        0,
                        Random.Range(0, LevelGenerator.current.segmentSize.y)                    
                        );                                
                }
                // turn RIGHT
                else if (exit.dir == MovementDirections.Right)
                {
                    exit.openingAddress = new Vector2Int(
                        LevelGenerator.current.segmentSize.x - 1,
                        Random.Range(0, LevelGenerator.current.segmentSize.y)                    
                        );                
                }
                // go STRAIGHT
                else if (exit.dir == MovementDirections.Front)
                {
                    exit.openingAddress = new Vector2Int(
                        Random.Range(0, LevelGenerator.current.segmentSize.x),
                        LevelGenerator.current.segmentSize.y  -1                  
                        );               
                }
            }
            // if entrance is FRONT
            else if (entrance.dir == MovementDirections.Front)
            {            
                // turn RIGHT
                if(exit.dir == MovementDirections.Left)
                {
                    exit.openingAddress = new Vector2Int(
                        0,
                        Random.Range(0, LevelGenerator.current.segmentSize.y)                    
                        );                
                }
                // turn LEFT
                else if (exit.dir == MovementDirections.Right)
                {
                    exit.openingAddress = new Vector2Int(
                        LevelGenerator.current.segmentSize.x - 1,
                        Random.Range(0, LevelGenerator.current.segmentSize.y)
                        );               
                }
                // go STRAIGHT
                else if (exit.dir == MovementDirections.Back)
                {
                    exit.openingAddress = new Vector2Int(
                        Random.Range(0, LevelGenerator.current.segmentSize.x),
                        0
                        );
                }
            }
            // if entrance is LEFT
            else if (entrance.dir == MovementDirections.Left)
            {           
                // turn RIGHT
                if (exit.dir == MovementDirections.Back)
                {
                    exit.openingAddress = new Vector2Int(
                        Random.Range(0, LevelGenerator.current.segmentSize.x),
                        0
                        );                
                }
                // turn LEFT
                else if (exit.dir == MovementDirections.Front)
                {
                    exit.openingAddress = new Vector2Int(
                        Random.Range(0, LevelGenerator.current.segmentSize.x),
                        LevelGenerator.current.segmentSize.y -1
                        );                
                }
                // go STRAIGHT
                else if (exit.dir == MovementDirections.Right)
                {
                    exit.openingAddress = new Vector2Int(
                        LevelGenerator.current.segmentSize.x -1 ,
                        Random.Range(0, LevelGenerator.current.segmentSize.y));                
                }
            }
            // if entrance is RIGHT
            else if (entrance.dir == MovementDirections.Right)
            {                
                // turn LEFT
                if (exit.dir == MovementDirections.Back)
                {
                    exit.openingAddress = new Vector2Int(
                        Random.Range(0, LevelGenerator.current.segmentSize.x),
                        0
                        );            
                }
                // turn RiGHT
                else if (exit.dir == MovementDirections.Front)
                {
                    exit.openingAddress = new Vector2Int(
                        Random.Range(0, LevelGenerator.current.segmentSize.x),
                        LevelGenerator.current.segmentSize.y - 1
                        );
                }
                // go STRAIGHT
                else if (exit.dir == MovementDirections.Left)
                {
                    exit.openingAddress = new Vector2Int(
                        0,
                        Random.Range(0, LevelGenerator.current.segmentSize.y)
                        );            
                }
            }                                                                                
        }
        // finalize first segment
        else if(exit.open)
        {
            if(exit.dir == MovementDirections.Front)
            {
                exit.openingAddress = new Vector2Int(
                       Random.Range(0, LevelGenerator.current.segmentSize.x),
                       LevelGenerator.current.segmentSize.y - 1
                       );
            }
            else if(exit.dir == MovementDirections.Right)
            {
                exit.openingAddress = new Vector2Int(
                       LevelGenerator.current.segmentSize.x - 1,
                       Random.Range(0, LevelGenerator.current.segmentSize.y)
                       );
            }
            else if(exit.dir == MovementDirections.Back)
            {
                exit.openingAddress = new Vector2Int(
                        Random.Range(0, LevelGenerator.current.segmentSize.x),
                        0
                        );
            }
            else if(exit.dir == MovementDirections.Left)
            {
                exit.openingAddress = new Vector2Int(
                       0,
                       Random.Range(0, LevelGenerator.current.segmentSize.y)
                       );
            }
        }
    }    
    
    public void FinilizeEndSegmentLayout()
    {        
        gameObject.name = "End Room";        
    }
    public void GenerateSegmentInterior()
    {        
        levelSegmentGenerator.GenerateWholeSegment(this);        
    }        

    private void DefineDictionery()
    {
        levelSegmentGenerator = GetComponent<LevelSegmentGenerator>();

        OpeningParameters parm_front = new OpeningParameters();
        OpeningParameters parm_right = new OpeningParameters();        
        OpeningParameters parm_back = new OpeningParameters();
        OpeningParameters parm_left = new OpeningParameters();

        parm_front.open = false;                
        parm_front.dir = MovementDirections.Front;

        parm_right.open = false;                
        parm_right.dir = MovementDirections.Right;

        parm_back.open = false;              
        parm_back.dir = MovementDirections.Back;

        parm_left.open = false;                
        parm_left.dir = MovementDirections.Left;

        openings.Add(MovementDirections.Front, parm_front);
        openings.Add(MovementDirections.Right, parm_right);        
        openings.Add(MovementDirections.Back, parm_back);
        openings.Add(MovementDirections.Left, parm_left);
    }
    
    public List<SegmentCell> GetCells() 
    {
        List<SegmentCell> segmentCellsToReturn = new List<SegmentCell>();
        SegmentCell[][] segmentCells = levelSegmentGenerator.GetCells();
        for(int i = 0; i < segmentCells.Length; ++i)
        {
            for(int j = 0; j < segmentCells[i].Length; ++j)
            {
                if(segmentCells[i][j] != null)
                    segmentCellsToReturn.Add(segmentCells[i][j]);
            }
        }
        return segmentCellsToReturn;
    }
    public SegmentCell GetRandomCell() {
        var _randomRoomIndex = Random.Range(0, levelSegmentGenerator.rooms.Count);
        return levelSegmentGenerator.rooms[_randomRoomIndex].GetRandomCellInRoom();
    }


    [System.Serializable]
    public struct OpeningParameters
    {        
        public bool open;
        public Vector2Int openingAddress;        
        public MovementDirections dir;
    }      
    
    public OpeningParameters GetExit() { return exit; }
    public OpeningParameters GetEntrance() { return entrance; }
}
