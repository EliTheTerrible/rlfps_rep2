﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.AI;

public class LevelGenerator : MonoBehaviour
{
    public static LevelGenerator current;
    
    [Header("Level Options")]
    public bool generateMap;
    public bool generateNavmesh;
    public bool generateLighting;
    public bool spawnEnemies;
    public bool randomizeSegmentType;
    public int segmentTypeToSpawn;
    [Header("Level Parametes")][Range(0f, 1f)]
    public float doorProbability;    
    public Vector2Int levelSize;
    public Vector2Int segmentSize;
    public float cellSizeFactor;
    public int minLevelLeangh, maxLevelLeangh;
    public int maxGenerationAttempts = 10;
    [Header("Segment Prefab Refrences")]
    public GameObject[] segmentPrefabs;    
    
    public LevelSegment[][] levelMap;
    LightmapSettings LS;
    Transform _transform;
    int segmentsCreated = 0;
    Vector2Int currentPosition = Vector2Int.zero;
    MovementDirections currentDirection;
    LevelSegment lastSegmentCreated;
    List<LevelSegment> segments = new List<LevelSegment>();
    bool deadEnd = false;
    [HideInInspector] public bool navmeshReady { get; private set; } = false;

    public void OnEnable()
    {                
        if (current != this)
        {
            if (current != null)
            {
                Destroy(current.gameObject);
            }
            current = this;
        }        
        _transform = transform;        
        levelMap = new LevelSegment[levelSize.x][];
        for (int i = 0; i < levelSize.x; ++i)
        {
            levelMap[i] = new LevelSegment[levelSize.y];
            for (int j = 0; j < levelSize.y; ++j)
            {
                levelMap[i][j] = null;
            }
        }        
    }

    [ContextMenu("Create level")]
    public IEnumerator CreateLevel()
    {
        StopAllCoroutines();
        if(LevelManager.current.player!=null) LevelManager.current.player.gameObject.SetActive(false);
        if (segments.Count != 0) {
            navmeshReady = false;
            ClearPath();
            NavMesh.RemoveAllNavMeshData();
        }

        if (maxLevelLeangh > 0) {
            CreatePath(0);
            PopulatePathSegments();
        }
        if(generateNavmesh) {            
           yield return StartCoroutine(GenerateNavMesh());
        }
        if(generateLighting) {            
        }
        if(generateMap)
        {
            yield return StartCoroutine(GenerateMap());
        }
        if (spawnEnemies) {
            yield return StartCoroutine(SpawnEnemies());
        }
        var startPosition = segments[0].GetCells()[0].transform.position + new Vector3(0f, 1f, 0f);
        if (LevelManager.current.player != null) {
            LevelManager.current.player.transform.position = startPosition;
            LevelManager.current.player.gameObject.SetActive(true);
        }
    }
    private void PopulatePathSegments()
    {
        for (int i = 0; i < segments.Count; ++i)
        {
            segments[i].GenerateSegmentInterior();
        }
    }
    private IEnumerator GenerateMap()
    {
        if (MapGenerator.current == null)
        {
            if (!_transform.parent.TryGetComponent<MapGenerator>(out MapGenerator.current))
            {
                Debug.Log("No Map Generator Found!");
            }
        }
        if (MapGenerator.current != null) MapGenerator.current.GenerateMap();
        yield return null;
    }
    private IEnumerator SpawnEnemies()
    {
        foreach (LevelSegment _segmentToPopulate in segments)
        {
            _segmentToPopulate.enemySpawner.PopulateSegment();
        }        
        yield return null;
    }

    [ContextMenu("Create Navmesh")]
    private IEnumerator GenerateNavMesh()
    {
        List<NavMeshSurface> levelSurfaces = new List<NavMeshSurface>();        
        foreach (LevelSegment segment in segments)
        {
            yield return new WaitUntil(() => segment.levelSegmentGenerator.done);
            List <SegmentCell> segmentCells = segment.GetCells();
            foreach (SegmentCell cell in segmentCells)
            {                
                var navmeshSurface = cell.GetComponentsInChildren<NavMeshSurface>();  
                if(navmeshSurface != null)
                    levelSurfaces.AddRange(navmeshSurface);                       
            }
        }  
        
        for (int i = 0; i < levelSurfaces.Count; ++i)
        {
            var _parent = levelSurfaces[i].transform.parent;            
            levelSurfaces[i].gameObject.isStatic = true;
            levelSurfaces[i].AddData();            
        }
        
        foreach(LevelSegment _segment in segments)
        {
            foreach(SegmentCell _cellToLink in _segment.GetCells())
            {
                if(_cellToLink is PremadeRoom)
                {
                    continue;
                }
                foreach(MovementDirections _dir in MazeDirections.AllDirections())
                {
                    var _edgeToLink = _cellToLink.GetEdge(_dir);
                    if (_cellToLink.GetEdge(_dir) is CellPassage)
                        _cellToLink.SetNMLink(_dir, false);
                    else if(_cellToLink.GetEdge(_dir) is CellDoor)
                    {
                        _cellToLink.SetNMLink(_dir, true);
                    }
                }
            }
        }
        navmeshReady = true;
        yield return null;
    }
    void CreatePath(int _levelsGenerated)
    {
        #region Loop Killswitch stuff
        int generationSteps = 0;
        int generationLimit = levelSize.x * levelSize.y;
        #endregion

        int levelsGenerated = _levelsGenerated +1;
        int targetLeangh = minLevelLeangh + Mathf.RoundToInt(Random.Range(0f,(maxLevelLeangh - minLevelLeangh)));
        
        CreateFirstSegment();
        segmentsCreated++;
        
        while (!deadEnd)
        {
            #region Loop Killswitch
            generationSteps++;
            if (generationSteps >= generationLimit)
            {
                Debug.Log("Generation Loop stuck, stoping...");
                break;
            }
            #endregion

            PickDirectionAny();
            if (deadEnd) break;
            
            if(segmentsCreated < targetLeangh)
            {                
                MoveInRandomDirection();
                if (lastSegmentCreated != null)
                {
                    lastSegmentCreated.FinilizeSegmentLayout(currentDirection, "Segment " + segmentsCreated);
                    
                }
                CreateNewSegment(currentDirection.GetOpposite());
                segmentsCreated++;
            }
            else
            {
                deadEnd = true;
                break;
            }
        }        
        lastSegmentCreated.FinilizeEndSegmentLayout();
        levelMap[currentPosition.x][currentPosition.y] = lastSegmentCreated;
        if (segments.Count < minLevelLeangh)
        {
            ClearPath();
            if(levelsGenerated == maxGenerationAttempts)
            {
                Debug.Log("Generator Crashed, cant build level");
                enabled = false;                
            }
            else
            {
                CreatePath(levelsGenerated);
                Debug.Log("Can't generate Level, regenerating Level");                
            }
            return;
        }        
    }
    private void MoveInRandomDirection()
    {
        MovementDirections originDirection = currentDirection.GetOpposite();
        if (currentDirection == MovementDirections.Front)
        {
            _transform.Translate(_transform.forward * segmentSize.y * cellSizeFactor);
            currentPosition.y += 1;
        }
        else if (currentDirection == MovementDirections.Right)
        {
            _transform.Translate(_transform.right * segmentSize.x * cellSizeFactor);
            currentPosition.x += 1;
        }
        else if (currentDirection == MovementDirections.Back)
        {
            _transform.Translate(-_transform.forward * segmentSize.y * cellSizeFactor);
            currentPosition.y -= 1;
        }
        else if (currentDirection == MovementDirections.Left)
        {
            _transform.Translate(-_transform.right * segmentSize.x * cellSizeFactor);
            currentPosition.x -= 1;
        }                        
    }

    private void CalibrateGenerator()
    {
        var startPosition = Vector3Int.zero; //new Vector3Int(Random.Range(0, levelSize.x), 0, Random.Range(0, levelSize.y));
        currentPosition = new Vector2Int(startPosition.x, startPosition.z);
        _transform.position = new Vector3((startPosition.x * segmentSize.x) + (segmentSize.x * cellSizeFactor / 2f), 0f, (startPosition.z * segmentSize.y) + (segmentSize.y * cellSizeFactor / 2f));        
    }
    private void CreateFirstSegment()
    {
        CalibrateGenerator();
        var segmenToSpawn = GetSegmentPrefabToSpawn();
        lastSegmentCreated = Instantiate(segmenToSpawn, _transform.position, Quaternion.identity, null).GetComponent<LevelSegment>();
        lastSegmentCreated.CreateFirstSegment();
        segments.Add(lastSegmentCreated);
        levelMap[currentPosition.x][currentPosition.y] = lastSegmentCreated;                        
        
    }
    private void CreateNewSegment(MovementDirections originDirection)
    {
        Vector2Int entranceCoord = lastSegmentCreated.GetExit().openingAddress;
        if(originDirection == MovementDirections.Front)
        {
            entranceCoord.y = segmentSize.y - 1;
        }
        else if(originDirection == MovementDirections.Right)
        {
            entranceCoord.x = segmentSize.x - 1;
        }
        else if (originDirection == MovementDirections.Back)
        {
            entranceCoord.y = 0;
        }
        else if (originDirection == MovementDirections.Left)
        {
            entranceCoord.x = 0;
        }
        var segmenToSpawn = GetSegmentPrefabToSpawn();
        lastSegmentCreated = Instantiate(segmenToSpawn, _transform.position, Quaternion.identity, null).GetComponent<LevelSegment>();
        lastSegmentCreated.CreateSegmentLayout(originDirection, entranceCoord);
        levelMap[currentPosition.x][currentPosition.y] = lastSegmentCreated;
        segments.Add(lastSegmentCreated);
    }
    private void PickDirectionAny()
    {
        // generator is in the middle of the map
        if (currentPosition.x != 0 && currentPosition.x < (levelSize.x - 1) && currentPosition.y != 0f && currentPosition.y < (levelSize.y - 1))
        {            
            List<MovementDirections> unblockedDirection = new List<MovementDirections>();
            bool blocked_l, blocked_r, blocked_f, blocked_b;
            
            blocked_l = levelMap[currentPosition.x - 1][currentPosition.y];
            blocked_r = levelMap[currentPosition.x + 1][currentPosition.y];
            blocked_f = levelMap[currentPosition.x][currentPosition.y + 1];
            blocked_b = levelMap[currentPosition.x][currentPosition.y - 1];

            if(!blocked_l)
            {
                unblockedDirection.Add(MovementDirections.Left);
            }
            if(!blocked_r)
            {
                unblockedDirection.Add(MovementDirections.Right);
            }
            if(!blocked_f)
            {
                unblockedDirection.Add(MovementDirections.Front);
            }
            if(!blocked_b)
            {
                unblockedDirection.Add(MovementDirections.Back);
            }
            SetDirection(unblockedDirection);
        }
        // generator is on the LEFT or RIGHT edge
        else if (currentPosition.x == 0 || currentPosition.x == levelSize.x - 1)
        {
            // generator is on LEFT edge
            if(currentPosition.x == 0)
            {
                // generator is in UPPER LEFT corner
                if(currentPosition.y == levelSize.y - 1)
                {
                    List<MovementDirections> unblockedDirection = new List<MovementDirections>();
                    bool blocked_r, blocked_b;

                    blocked_r = levelMap[currentPosition.x + 1][currentPosition.y];
                    blocked_b = levelMap[currentPosition.x][currentPosition.y - 1];

                    if (!blocked_r)
                    {
                        unblockedDirection.Add(MovementDirections.Right);
                    }
                    if (!blocked_b)
                    {
                        unblockedDirection.Add(MovementDirections.Back);
                    }
                    SetDirection(unblockedDirection);
                }
                // generator is in LOWER LEFT corner
                else if(currentPosition.y == 0)
                {
                    List<MovementDirections> unblockedDirection = new List<MovementDirections>();
                    bool blocked_r, blocked_f;
                    
                    blocked_r = levelMap[currentPosition.x + 1][currentPosition.y];
                    blocked_f = levelMap[currentPosition.x][currentPosition.y + 1];
                    
                    if (!blocked_r)
                    {
                        unblockedDirection.Add(MovementDirections.Right);
                    }
                    if (!blocked_f)
                    {
                        unblockedDirection.Add(MovementDirections.Front);
                    }
                    SetDirection(unblockedDirection);
                }
                // generator is in middle of LEFT edge
                else
                {
                    List<MovementDirections> unblockedDirection = new List<MovementDirections>();
                    bool blocked_r, blocked_f, blocked_b;
                    
                    blocked_r = levelMap[currentPosition.x + 1][currentPosition.y];
                    blocked_f = levelMap[currentPosition.x][currentPosition.y + 1];
                    blocked_b = levelMap[currentPosition.x][currentPosition.y - 1];

                    if (!blocked_r)
                    {
                        unblockedDirection.Add(MovementDirections.Right);
                    }
                    if (!blocked_f)
                    {
                        unblockedDirection.Add(MovementDirections.Front);
                    }
                    if (!blocked_b)
                    {
                        unblockedDirection.Add(MovementDirections.Back);
                    }
                    SetDirection(unblockedDirection);
                }
            }
            // generator is on RIGHT edge
            else if(currentPosition.x == levelSize.x - 1)
            {
                // generator is in UPPER RIGHT corner
                if (currentPosition.y == levelSize.y - 1)
                {
                    List<MovementDirections> unblockedDirection = new List<MovementDirections>();
                    bool blocked_l, blocked_b;

                    blocked_l = levelMap[currentPosition.x - 1][currentPosition.y];                    
                    blocked_b = levelMap[currentPosition.x][currentPosition.y - 1];

                    if (!blocked_l)
                    {
                        unblockedDirection.Add(MovementDirections.Left);
                    }                    
                    if (!blocked_b)
                    {
                        unblockedDirection.Add(MovementDirections.Left);
                    }
                    SetDirection(unblockedDirection);
                }
                // generator is in LOWER RIGHT corner
                else if (currentPosition.y == 0)
                {
                    List<MovementDirections> unblockedDirection = new List<MovementDirections>();
                    bool blocked_l, blocked_f;

                    blocked_l = levelMap[currentPosition.x - 1][currentPosition.y];                    
                    blocked_f = levelMap[currentPosition.x][currentPosition.y + 1];                    

                    if (!blocked_l)
                    {
                        unblockedDirection.Add(MovementDirections.Left);
                    }                    
                    if (!blocked_f)
                    {
                        unblockedDirection.Add(MovementDirections.Front);
                    }
                    SetDirection(unblockedDirection);
                }
                // generator is in middle of RIGHT edge
                else
                {
                    List<MovementDirections> unblockedDirection = new List<MovementDirections>();
                    bool blocked_l, blocked_f, blocked_b;

                    blocked_l = levelMap[currentPosition.x - 1][currentPosition.y];                    
                    blocked_f = levelMap[currentPosition.x][currentPosition.y + 1];
                    blocked_b = levelMap[currentPosition.x][currentPosition.y - 1];

                    if (!blocked_l)
                    {
                        unblockedDirection.Add(MovementDirections.Left);
                    }                    
                    if (!blocked_f)
                    {
                        unblockedDirection.Add(MovementDirections.Front);
                    }
                    if (!blocked_b)
                    {
                        unblockedDirection.Add(MovementDirections.Back);
                    }
                    SetDirection(unblockedDirection);
                }
            }
        }
        // generator is on UPPER or LOWER edge
        else if (currentPosition.y == 0 || currentPosition.y == levelSize.y - 1)
        {
            //generator is on UPPER edge
            if(currentPosition.y == levelSize.y - 1)
            {                
                // generator is in UPPER RIGHT corner
                if (currentPosition.x == levelSize.x - 1)
                {
                    List<MovementDirections> unblockedDirection = new List<MovementDirections>();
                    bool blocked_l, blocked_b;

                    blocked_l = levelMap[currentPosition.x - 1][currentPosition.y];
                    blocked_b = levelMap[currentPosition.x][currentPosition.y - 1];

                    if (!blocked_l)
                    {
                        unblockedDirection.Add(MovementDirections.Left);
                    }

                    if (!blocked_b)
                    {
                        unblockedDirection.Add(MovementDirections.Back);
                    }
                    SetDirection(unblockedDirection);
                }                
                // generator is in UPPER LEFT corner
                else if(currentPosition.x == 0)
                {
                    List<MovementDirections> unblockedDirection = new List<MovementDirections>();
                    bool blocked_r, blocked_b;

                    blocked_r = levelMap[currentPosition.x + 1][currentPosition.y];
                    blocked_b = levelMap[currentPosition.x][currentPosition.y - 1];

                    if (!blocked_r)
                    {
                        unblockedDirection.Add(MovementDirections.Right);
                    }
                    if (!blocked_b)
                    {
                        unblockedDirection.Add(MovementDirections.Back);
                    }
                    SetDirection(unblockedDirection);
                }
                // generator is in middle of UPPER edge
                else
                {
                    List<MovementDirections> unblockedDirection = new List<MovementDirections>();
                    bool blocked_l, blocked_r, blocked_b;

                    blocked_l = levelMap[currentPosition.x - 1][currentPosition.y];
                    blocked_r = levelMap[currentPosition.x + 1][currentPosition.y];                    
                    blocked_b = levelMap[currentPosition.x][currentPosition.y - 1];

                    if (!blocked_l)
                    {
                        unblockedDirection.Add(MovementDirections.Left);
                    }
                    if (!blocked_r)
                    {
                        unblockedDirection.Add(MovementDirections.Right);
                    }                    
                    if (!blocked_b)
                    {
                        unblockedDirection.Add(MovementDirections.Back);
                    }
                    SetDirection(unblockedDirection);
                }
            }
            // generator is on LOWER edge
            else if (currentPosition.y == 0)
            {
                // generator is in LOWER RIGHT corner
                if (currentPosition.x == levelSize.x - 1)
                {
                    List<MovementDirections> unblockedDirection = new List<MovementDirections>();
                    bool blocked_l, blocked_f;

                    blocked_l = levelMap[currentPosition.x - 1][currentPosition.y];
                    blocked_f = levelMap[currentPosition.x][currentPosition.y + 1];

                    if (!blocked_l)
                    {
                        unblockedDirection.Add(MovementDirections.Left);
                    }
                    if (!blocked_f)
                    {
                        unblockedDirection.Add(MovementDirections.Front);
                    }
                    SetDirection(unblockedDirection);
                }
                // generator is in LOWER LEFT corner
                else if (currentPosition.x == 0)
                {
                    List<MovementDirections> unblockedDirection = new List<MovementDirections>();
                    bool blocked_r, blocked_f;

                    blocked_r = levelMap[currentPosition.x + 1][currentPosition.y];
                    blocked_f = levelMap[currentPosition.x][currentPosition.y + 1];

                    if (!blocked_r)
                    {
                        unblockedDirection.Add(MovementDirections.Right);
                    }
                    if (!blocked_f)
                    {
                        unblockedDirection.Add(MovementDirections.Front);
                    }
                    SetDirection(unblockedDirection);
                }
                // generator is in middle of LOWER edge
                else
                {
                    List<MovementDirections> unblockedDirection = new List<MovementDirections>();
                    bool blocked_l, blocked_r, blocked_f;

                    blocked_l = levelMap[currentPosition.x - 1][currentPosition.y];
                    blocked_r = levelMap[currentPosition.x + 1][currentPosition.y];
                    blocked_f = levelMap[currentPosition.x][currentPosition.y + 1];                    

                    if (!blocked_l)
                    {
                        unblockedDirection.Add(MovementDirections.Left);
                    }
                    if (!blocked_r)
                    {
                        unblockedDirection.Add(MovementDirections.Right);
                    }
                    if (!blocked_f)
                    {
                        unblockedDirection.Add(MovementDirections.Front);
                    }
                    SetDirection(unblockedDirection);
                }
            }
        }
    }
    private void SetDirection(List<MovementDirections> unblockedDirection)
    {
        if(unblockedDirection.Count == 0)
        {
            deadEnd = true;
            return;
        }
        currentDirection = unblockedDirection[Random.Range(0, unblockedDirection.Count)];
    }           
    private void ClearPath()
    {
        for (int i = 0; i < segmentsCreated; i++)
        {
            Destroy(segments[0].gameObject);
            segments.RemoveAt(0);
        }

        for (int i = 0; i < levelMap.Length; ++i)
        {
            for (int j = 0; j < levelMap[i].Length; ++j)
            {
                levelMap[i][j] = null;
                //MapGenerator.current.FlipCell(i,j, false);
            }
        }
        segmentsCreated = 0;
        deadEnd = false;
    }

    public List<LevelSegment> GetSegments() {
        return segments;
    }
    public Vector2 GetSegmentAddress(LevelSegment _segmentToFInd) {        
        for(int x =0; x < levelMap.Length; ++x)
        {
            for(int z = 0; z < levelMap[x].Length; ++z)
            {
                if(levelMap[x][z] == _segmentToFInd)
                {
                    return new Vector2(x, z);
                }
            }
        }
        return -Vector2.one;
    }
    public LevelSegment GetRandomSegmnet() {        
        var _randomSegmentIndex = Random.Range(0, segments.Count);
        return segments[_randomSegmentIndex];
    }
    private GameObject GetSegmentPrefabToSpawn()
    {
        GameObject newSegmentPrefab;
        if(randomizeSegmentType)
        {
            var randomPrefabIndex = Random.Range(0, segmentPrefabs.Length);
            newSegmentPrefab = segmentPrefabs[randomPrefabIndex];
        }
        else
        {
            newSegmentPrefab = segmentPrefabs[segmentTypeToSpawn];
        }
        return newSegmentPrefab;
    }
}
