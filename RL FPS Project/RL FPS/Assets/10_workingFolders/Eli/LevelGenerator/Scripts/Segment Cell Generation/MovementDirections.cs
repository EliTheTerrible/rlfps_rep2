﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public enum MovementDirections {
	Front, // Front
	Right,  // Right
	Back, // Back
	Left   // Left
}

public static class MazeDirections {

	public const int Count = 4;
    public static MovementDirections[] AllDirections() {
        MovementDirections[] all = { MovementDirections.Front, MovementDirections.Right, MovementDirections.Back, MovementDirections.Left};
        return all;
    }
    // set to a random direction
	public static MovementDirections RandomValue {
		get {
			return (MovementDirections)Random.Range(0, Count);
		}
	}
    
    // a static array of direction enums, defining for each direction its opposite
	private static MovementDirections[] opposites = {
		MovementDirections.Back,
		MovementDirections.Left,
		MovementDirections.Front,
		MovementDirections.Right
	};

	public static MovementDirections GetOpposite (this MovementDirections direction) {
		return opposites[(int)direction];
	}
   
    // return the next clockwise direction relative to the selected direction
    public static MovementDirections GetNextClockwise (this MovementDirections direction) {
		return (MovementDirections)(((int)direction + 1) % Count);
	}


    // return the next counter-clockwise direction relative to the selected direction
    public static MovementDirections GetNextCounterclockwise (this MovementDirections direction) {
		return (MovementDirections)(((int)direction + Count - 1) % Count);
	}
	
    // a static array of directions, tranformed to float values, in the order they appear in the Enum decleration (front, right, back, left), ccw from front
	private static Vector2Int[] vectors = {
		new Vector2Int(0, 1),
		new Vector2Int(1, 0),
		new Vector2Int(0, -1),
		new Vector2Int(-1, 0)
	};
    // convert a global direction to relative to a specified one
    public static MovementDirections RelativeTo(this MovementDirections direction, MovementDirections directionOfRefrence)
    {
        MovementDirections localDir = direction;        
        if(directionOfRefrence == MovementDirections.Right)
        {
            localDir = localDir.GetNextClockwise();
        }
        if(directionOfRefrence == MovementDirections.Back)
        {
            localDir = localDir.GetNextClockwise();
            localDir = localDir.GetNextClockwise();
        }
        if(directionOfRefrence == MovementDirections.Left)
        {
            localDir = localDir.GetNextCounterclockwise();
        }
        return localDir;
    }
    // rotate a grid of vector2int to a specified direction
    public static Vector2Int AlignGrid(this MovementDirections directions, Vector2Int grid)
    {
        if (directions == MovementDirections.Front)
            return grid;
        else if (directions == MovementDirections.Right)
            return new Vector2Int(grid.y, -grid.x);
        else if (directions == MovementDirections.Back)
            return new Vector2Int(-grid.x, -grid.y);
        else
            return new Vector2Int(-grid.y, grid.x);

    }
	// return the vector direction of the selected direction
	public static Vector2Int ToIntVector2 (this MovementDirections direction) {
		return vectors[(int)direction];
	}
    // a static array of rotation defitions for each direction, arrenget at the same order as in the Enum decleration
	private static Quaternion[] rotations = {
		Quaternion.identity,
		Quaternion.Euler(0f, 90f, 0f),
		Quaternion.Euler(0f, 180f, 0f),
		Quaternion.Euler(0f, 270f, 0f)
	};

    public static float[] rotationsUI = {
        0f,
        -90f,
        180f,
        90f
    };
	// return the correct quaternion rotaion of the selected direction for walls
	public static Quaternion ToRotation (this MovementDirections direction) {
		return rotations[(int)direction];
	}      
    public static float ToRotationUI(this MovementDirections direction) {
        return rotationsUI[(int)direction];
    }
    public static float ToOppisiteRotationUI(this MovementDirections direction)
    {
        return rotationsUI[(int)direction] + 180f;
    }
}

