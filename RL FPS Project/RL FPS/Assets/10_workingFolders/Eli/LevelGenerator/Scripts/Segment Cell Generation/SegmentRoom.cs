﻿using UnityEngine;
using System.Collections.Generic;

public class SegmentRoom : ScriptableObject
{

    public int settingsIndex;
    public string RoomName;
    public SegmentRoomSettings settings;
    public bool visible = false;

    List<GameObject> entitiesInRoom = new List<GameObject>();
    List<NeigbhorInfo> neigbhorInfoList = new List<NeigbhorInfo>();
    [HideInInspector] public LevelSegmentGenerator generator;
    List<MovementDirections> allDir = new List<MovementDirections>();
    List<GameObject> roomMapObjects = new List<GameObject>();
    [HideInInspector] public List<SegmentCell> cells = new List<SegmentCell>();

    // define a cell as part of room
    public void Add(SegmentCell cell)
    {
        cell.room = this;
        cells.Add(cell);
    }

    // combine two rooms together 
    public void Assimilate(SegmentRoom room)
    {
        for (int i = 0; i < room.cells.Count; i++)
        {
            Add(room.cells[i]);
        }
    }

    public void AssignMapObjects(List<GameObject> _mapObjects)
    {
        roomMapObjects = null;
        roomMapObjects = _mapObjects;
    }

    public void HideOnMap()
    {
        foreach (GameObject _object in roomMapObjects)
        {
            _object.SetActive(false);
        }
    }
    public void ShowOnMap()
    {
        foreach(GameObject _object in roomMapObjects) {
            _object.SetActive(true);
        }
    }

    public SegmentCell GetRandomCellInRoom()
    {
        var _randomCellIndex = Random.Range(0, cells.Count);
        return cells[_randomCellIndex];
    }
    public SegmentRoom GetRandomRoomInSegment()
    {
        var _randomRoomIndex = Random.Range(0, generator.rooms.Count);
        return generator.rooms[_randomRoomIndex];
    }
    public void AccessCheck()
    {
        if (cells.Count == 0) return;

        #region temp list of all directions -allDirections-            
        allDir.Add(MovementDirections.Front);
        allDir.Add(MovementDirections.Right);
        allDir.Add(MovementDirections.Back);
        allDir.Add(MovementDirections.Left);
        #endregion
        generator = cells[0].generator;       


        neigbhorInfoList = GetNeigbhorInfoList();
        for(int i = 0; i < neigbhorInfoList.Count; ++i )
        {
            bool connected = CheckForConnection(neigbhorInfoList[i].neighbhorRoom);
            if (!connected)
            {
                int randomDoorPosition = Random.Range(0, neigbhorInfoList[i].neigbhoringCells.Count);
                var randomDoorCell = GetNeigbhorInfo(neigbhorInfoList[i].neighbhorRoom).neigbhoringCells[randomDoorPosition];
                foreach (MovementDirections dir in allDir)
                {
                    var newDoorCoordinates = randomDoorCell.coordinates + dir.ToIntVector2();
                    if (generator.ContainsCoordinates(newDoorCoordinates))
                    {
                        var neigbhorCell = generator.GetCell(newDoorCoordinates);
                        if (!(neigbhorCell is SegmentCell))
                            continue;
                        if (neigbhorCell.room == neigbhorInfoList[i].neighbhorRoom)
                        {
                            generator.CreateAssuredPassage(randomDoorCell, neigbhorCell, dir);                                                        
                            neigbhorInfoList[i].SetConnected();
                        } 
                    }
                }
            }
        }
    }
    List<NeigbhorInfo> GetNeigbhorInfoList()
    {
        List<NeigbhorInfo> _newNeigbhorInfoList = new List<NeigbhorInfo>();
        foreach (SegmentCell _cellToCheck in cells)
        {
            foreach (MovementDirections _dir in allDir)
            {
                var _neigbhorCellCoord = _cellToCheck.coordinates + _dir.ToIntVector2();
                if (!generator.ContainsCoordinates(_neigbhorCellCoord))
                    continue; // neighbhor in direction dir is outside of segment


                var _neigbhorCell = generator.GetCell(_neigbhorCellCoord);
                if(_neigbhorCell == null || generator.GetCell(_neigbhorCellCoord).room == null)
                {
                    continue;
                }
                if (_neigbhorCell.room == this)
                    continue; // neighbhor in direction dir is part of the same room                
                
                if (NeigbhorInfoExists(_newNeigbhorInfoList, _neigbhorCell.room))
                {
                    var _neigbhorInfo = GetNeigbhorInfo(_newNeigbhorInfoList, _neigbhorCell.room);                    
                    _neigbhorInfo.neigbhoringCells.Add(_cellToCheck);                    
                }
                else
                {                    
                    NeigbhorInfo _newNeigbhor = new NeigbhorInfo();
                    _newNeigbhor.neighbhorRoom = _neigbhorCell.room;
                    _newNeigbhor.neigbhoringCells = new List<SegmentCell>();
                    _newNeigbhor.neigbhoringCells.Add(_cellToCheck);
                    _newNeigbhorInfoList.Add(_newNeigbhor);
                }

            }
        }
        return _newNeigbhorInfoList;
    }
    bool NeigbhorInfoExists(List<NeigbhorInfo> _list, SegmentRoom _neigbhorRoom)
    {        
        foreach (NeigbhorInfo _neigbhorToCheck in _list) {            
            if (_neigbhorToCheck.neighbhorRoom.RoomName == _neigbhorRoom.RoomName) {
                return true;
            }
        }        
        return false;
    }
    bool CheckForConnection(SegmentRoom _neigbhorToCheck)
    {
        LevelSegment.OpeningParameters entrance = generator.parentSegment.GetEntrance();
        LevelSegment.OpeningParameters exit = generator.parentSegment.GetExit();

        bool _connected = false;
        List<SegmentCell> _cellsToCheck = GetNeigbhorInfo(_neigbhorToCheck).neigbhoringCells;
        foreach (SegmentCell edgeCellToCheck in _cellsToCheck) {
            foreach (MovementDirections dir in allDir) {
                if (entrance.openingAddress == edgeCellToCheck.coordinates && dir == entrance.dir && entrance.open)
                    continue;
                if (exit.openingAddress == edgeCellToCheck.coordinates && dir == exit.dir && exit.open)
                    continue;

                var neigbhorCoord = edgeCellToCheck.coordinates + dir.ToIntVector2();
                if (generator.ContainsCoordinates(neigbhorCoord)) {
                    var neigbhorCell = generator.GetCell(neigbhorCoord);
                    if (!(neigbhorCell is SegmentCell))
                        continue;
                    if (neigbhorCell.room == _neigbhorToCheck) {
                        var cellEdge = edgeCellToCheck.GetEdge(dir) as CellDoor;
                        if (cellEdge != null)
                            _connected = true;
                    }
                }
            }
        }
        return _connected;
    }
    
    public struct NeigbhorInfo
    {        
        public SegmentRoom neighbhorRoom;
        public List<SegmentCell> neigbhoringCells;
        public bool connected;
        public bool _empty;
        public void SetConnected()
        {
            connected = true;
        }
    }
    public NeigbhorInfo GetNeigbhorInfo(List<NeigbhorInfo> _list, SegmentRoom _roomToPick)
    {
        NeigbhorInfo _infoToReturn = new NeigbhorInfo();
        _infoToReturn._empty = true;

        foreach (NeigbhorInfo _info in _list)
        {
            if (_info.neighbhorRoom.RoomName == _roomToPick.RoomName)
            {
                _infoToReturn = _info;
                _infoToReturn._empty = false;
            }
        }
        return _infoToReturn;
    }
    public NeigbhorInfo GetNeigbhorInfo(SegmentRoom _roomToPick)
    {
        NeigbhorInfo _edgeToReturn = new NeigbhorInfo();
        _edgeToReturn._empty = true;
        foreach (NeigbhorInfo _edge in neigbhorInfoList)
        {
            if (_edge.neighbhorRoom.RoomName == _roomToPick.RoomName)
            {
                _edgeToReturn = _edge;
                _edgeToReturn._empty = false;
            }
        }
        return _edgeToReturn;
    }   
}