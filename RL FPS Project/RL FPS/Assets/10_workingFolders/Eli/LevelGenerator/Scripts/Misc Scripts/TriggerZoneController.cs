﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class TriggerZoneController : MonoBehaviour
{
    public string[] triggerTags;
    public UnityEvent OnTriggerEnterEvent;
    public UnityEvent OnTriggerExitEvent;
    public bool playerInZone;


    private void OnTriggerEnter(Collider other)    
    {
        playerInZone = true;
        foreach(string tagToCheck in triggerTags) 
        {
            if (other.gameObject.tag == tagToCheck)
            {            
                OnTriggerEnterEvent.Invoke();
            }        
        }
    }
    private void OnTriggerExit(Collider other)
    {
        playerInZone = false;
        foreach (string tagToCheck in triggerTags)
        {
            if (other.gameObject.tag == tagToCheck)
            {
                OnTriggerExitEvent.Invoke();
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, GetComponent<SphereCollider>().radius);
    }
}


