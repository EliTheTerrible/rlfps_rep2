﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PauseController : MonoBehaviour
{
    public static PauseController current;
    public bool pause { get; private set; }
    public bool _override;
    public GameObject systemPage, mapPage, inventoryPage;

    public void OnEnable()
    {
        if(current != this)
        {
            if(current != null)
            {
                current.enabled = false;
                Debug.LogWarning("Double Pause Controllers!");
            }
            current = this;
        }
        Time.timeScale = 1f;
        pause = false;
        SelectSystemPage();        
    }    
    public void PauseGame()
    {
        pause = true;
        SetPause(pause);
    }
    public void ResumeGame()
    {
        pause = false;        
        SetPause(pause);
    }

    private void SetPause(bool _isPaused)
    {
        Time.timeScale =  _isPaused ? 0f : 1f;
        pause = _isPaused;
        Cursor.lockState = _isPaused ? CursorLockMode.Confined : CursorLockMode.Locked;
        Cursor.visible = _isPaused;
        LevelManager.current.player.SetPuase(_isPaused);                
    }
    public void ReturnToMainMenu()
    {
        SceneTransitionController.current.TransitionToMainMenu();
    }
    public void ResetRun()
    {
        ResumeGame();
        if (SceneTransitionController.current != null) {
            List<GameObject> DDOLobjects = new List<GameObject>();
            SceneTransitionController.current.StartTransion("GeneratedLevel", null);
        }        
    }

    public void SelectSystemPage()
    {
        systemPage.SetActive(true);
        inventoryPage.SetActive(false);
        mapPage.SetActive(false);
    }
    public void SelectMapPage()
    {
        mapPage.SetActive(true);

        systemPage.SetActive(false);
        inventoryPage.SetActive(false);
    }
    public void SelectinventoryPage()
    {
        inventoryPage.SetActive(true);

        systemPage.SetActive(false);
        mapPage.SetActive(false);
    }

    public void exit()
    {
        Application.Quit();
    }

    public void ResetLevel()
    {
        RunManager.current.resetLevel();
    }
}
