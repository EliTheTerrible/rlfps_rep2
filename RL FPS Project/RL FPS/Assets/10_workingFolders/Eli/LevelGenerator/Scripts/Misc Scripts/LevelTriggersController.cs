﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class LevelTriggersController : MonoBehaviour
{
    public static LevelTriggersController current;

    public string levelGenerationSceneName;
    public GameObject Player;
    List<TriggerZoneController> triggerZones = new List<TriggerZoneController>();    
    public float zonePromptFadeTime;
    public TMP_Text zonePromptText;

    private void OnEnable()
    {
        Invoke("DelayedOnEnable", 0.1f);
    }

    private void DelayedOnEnable()
    {
        if (current != this)
        {
            if (current != null)
            {
                Destroy(this.gameObject);
                return;
            }
            current = this;
        }
        Player = GameObject.FindGameObjectWithTag("Player");
        zonePromptText = GameObject.Find("PromptText").GetComponent<TMP_Text>();                
        foreach(GameObject exitZone in GameObject.FindGameObjectsWithTag("Exit Zone"))
        {
            triggerZones.Add(exitZone.GetComponent<TriggerZoneController>());
            triggerZones[triggerZones.Count - 1].OnTriggerEnterEvent.AddListener(PlayerInExitZone());
            triggerZones[triggerZones.Count - 1].OnTriggerEnterEvent.AddListener(CheckFadeOutTrigger());
        }
    }

    private void Update()
    {
        foreach(TriggerZoneController triggerZone in triggerZones)
        {
            if (triggerZone.playerInZone && Input.GetKeyDown(KeyCode.E))
            {
                if (SceneTransitionController.current != null)
                {
                    List<GameObject> player = new List<GameObject>();
                    player.Add(Player);
                    SceneTransitionController.current.StartTransion(levelGenerationSceneName, player);
                }
            }
        }
    }    
    public UnityAction PlayerInExitZone()
    {        
        StopAllCoroutines();
        StartCoroutine(ZonePromptFadeIn("Press E to Load Next Level"));
        return null;
    }

    public void AddExitZone(GameObject newExitZone)
    {
        TriggerZoneController newTrigger = newExitZone.GetComponent<TriggerZoneController>();
        if(newTrigger != null)
        {
            triggerZones.Add(newTrigger);
        }
        else
        {
            Debug.Log("Bad Exit Zone");
        }
    }

    public UnityAction CheckFadeOutTrigger()
    {
        bool playerOutOfZones = true;
        foreach (TriggerZoneController TZ_controller in triggerZones)
        {
            if (TZ_controller.playerInZone)
            {
                playerOutOfZones = false;
            }
        }
        if (playerOutOfZones)
        {
            StopAllCoroutines();
            StartCoroutine(ZonePromptFadeOut());
        }
        return null;
    }


    IEnumerator ZonePromptFadeIn(string text)
    {
        zonePromptText.text = text;
        float t = Mathf.InverseLerp(0f, zonePromptFadeTime, zonePromptText.alpha);
        while (t <= zonePromptFadeTime)
        {
            zonePromptText.alpha = Mathf.Lerp(0f, 1f, t / zonePromptFadeTime);
            t += Time.unscaledDeltaTime;
            yield return null;
        }
    }

    IEnumerator ZonePromptFadeOut()
    {
        float t = Mathf.InverseLerp(0f, zonePromptFadeTime, zonePromptText.alpha);
        while (t >= 0f)
        {
            zonePromptText.alpha = Mathf.Lerp(0f, 1f, t / zonePromptFadeTime);
            t -= Time.unscaledDeltaTime;
            yield return null;
        }
    }


}
