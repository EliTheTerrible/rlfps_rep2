﻿using UnityEngine;
using TMPro;
using System.Collections;
using System.Collections.Generic;

public class HubController : MonoBehaviour
{
    public string levelGenerationSceneName;
    public GameObject Player;
    List<TriggerZoneController> triggerZones = new List<TriggerZoneController>();
    public TriggerZoneController bunkerTriggerZone;
    public TriggerZoneController shopTriggerZone;
    public float zonePromptFadeTime;
    public TMP_Text zonePromptText;

    private void OnEnable()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        triggerZones.Add(bunkerTriggerZone);
        triggerZones.Add(shopTriggerZone);
    }
    private void Update()
    {
        if(bunkerTriggerZone.playerInZone && Input.GetKeyDown(KeyCode.E))
        {
            if(SceneTransitionController.current != null)
            {
                ZonePromptFadeOut();
                List<GameObject> player = new List<GameObject>();
                player.Add(Player);
                SceneTransitionController.current.StartTransion(levelGenerationSceneName, player);
            }            
        }
    }

    public void PlayerInShopZone()
    {
        StopAllCoroutines();
        StartCoroutine(ZonePromptFadeIn("Press E to open the shop"));
    }
    public void PlayerInBunkerZone()
    {
        StopAllCoroutines();
        StartCoroutine(ZonePromptFadeIn("Press E to enter the bunker"));
    }   


    public void CheckFadeOutTrigger()
    {
        bool playerOutOfZones = true;
        foreach(TriggerZoneController TZ_controller in triggerZones)
        {
            if(TZ_controller.playerInZone)
            {
                playerOutOfZones = false;
            }
        }
        if(playerOutOfZones)
        {
            StopAllCoroutines();
            StartCoroutine(ZonePromptFadeOut());
        }
    }


    IEnumerator ZonePromptFadeIn(string text)
    {
        
        zonePromptText.text = text;
        float t = Mathf.InverseLerp(0f, zonePromptFadeTime, zonePromptText.alpha);
        while(t <= zonePromptFadeTime)
        {
            zonePromptText.alpha = Mathf.Lerp(0f, 1f, t/zonePromptFadeTime);
            t += Time.unscaledDeltaTime;
            yield return null;
        }
    }

    IEnumerator ZonePromptFadeOut()
    {        
        float t = Mathf.InverseLerp(0f, zonePromptFadeTime, zonePromptText.alpha);
        while (t >= 0f)
        {
            zonePromptText.alpha = Mathf.Lerp(0f, 1f, t/ zonePromptFadeTime);
            t -= Time.unscaledDeltaTime;
            yield return null;
        }
    }


}
