﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SegmentEnemyPopulator : MonoBehaviour
{
    LevelSegment parentSegmnet;
    public EnemySettings[] enemySettingsPresets;
    public EnemyController[] enemyPrefabs;
    public int minimumRoomSize;
    public int maxEnemiesInRoom;
    public void PopulateSegment()
    {
        if (parentSegmnet == null)
        {
            parentSegmnet = GetComponent<LevelSegment>();
        }
        SpawnInCorridors();
        SpawnInRooms();        
    }

    private void SpawnInRooms()
    {
        foreach (PremadeRoom _roomToPopulate in parentSegmnet.levelSegmentGenerator.premadeRooms)
        {
            foreach (Transform _enemySpawnpoint in _roomToPopulate.enemySpawns)
            {
                var newEnemy = Instantiate(enemyPrefabs[Random.Range(0, enemyPrefabs.Length)].gameObject, _enemySpawnpoint.transform.position, Quaternion.identity, null).GetComponent<EnemyController>();
                newEnemy.Spawn(enemySettingsPresets[0]);
            }
        }
    }

    private void SpawnInCorridors()
    {
        foreach (SegmentRoom _roomToPopulate in parentSegmnet.levelSegmentGenerator.rooms)
        {
            if (_roomToPopulate.cells.Count > minimumRoomSize)
            {
                if (_roomToPopulate.cells.Count > maxEnemiesInRoom)
                {
                    var _occupiedCells = new List<Vector2Int>();
                    for (int i = 0; i < maxEnemiesInRoom; ++i)
                    {
                        var randomCell = Random.Range(0, _roomToPopulate.cells.Count);
                        while (_occupiedCells.Contains(_roomToPopulate.cells[randomCell].coordinates))
                        {
                            randomCell = Random.Range(0, _roomToPopulate.cells.Count);
                        }
                        _occupiedCells.Add(_roomToPopulate.cells[randomCell].coordinates);
                        EnemyController newEnemy = Instantiate(enemyPrefabs[Random.Range(0, enemyPrefabs.Length)].gameObject, _roomToPopulate.cells[randomCell].transform.position, Quaternion.identity, null).GetComponent<EnemyController>();
                        newEnemy.Spawn(enemySettingsPresets[0]);                        
                    }
                }
                else
                {

                }
            }
        }
    }
}
