﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Chase : MonoBehaviour
{
    public GameObject fpsController;

    private Transform AI;
    public Transform FpsC;
    public Transform/*[] */moveSpots;


    private AudioSource sound;
    public AudioClip[] footSounds;
    private Animator anim;
    private NavMeshAgent nav;


    //Float
    [SerializeField] private float moveSpeed= 1.2f;
    [SerializeField] private float animSpeed= 1.2f;
    [SerializeField] private float aggroDistance= 12f;

   

    private float waitTime;
     public float startWaitTime;

    //Patrolling Second Methode
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;

    //Int
    //Patrolling first methode int
    /*  private int randomSpot;*/



    private void Start()
    {
        nav = GetComponent<NavMeshAgent>();
        sound = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        nav.speed = moveSpeed;
        anim.speed = animSpeed;

        waitTime = startWaitTime;
        moveSpots.position = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY));

        //for the other patrolling way
        /*  randomSpot = Random.Range(0, moveSpots.Length);*/



    }

    public void FootStep( int _num)
    {   

        sound.clip = footSounds[_num];
        sound.Play();
    }

    private void Update()
    {
        float distance = Vector3.Distance(transform.position, FpsC.position);
        


        if (distance < aggroDistance)
            Chasing();
        else
            Patrolling(); 

       
    }
    private void Chasing()
    {
            anim.SetFloat("velocity", nav.velocity.magnitude);
            nav.SetDestination(fpsController.transform.position);
      

    }
  /*  private void Patrolling()
    { float distance = Vector3.Distance(transform.position, moveSpots[randomSpot].position);

        Debug.Log("Is Patrolling");

        transform.position = Vector3.MoveTowards(transform.position, moveSpots[randomSpot].position, moveSpeed * Time.deltaTime);
       
        if (distance<0.2f)
        {
            if (waitTime <= 0)
            {
                randomSpot = Random.Range(0, moveSpots.Length);
                waitTime = startWaitTime;
            }
            else
            {
                waitTime -= Time.deltaTime;
            }
        }
    }
*/
  private void Patrolling()
    {
        anim.SetFloat("velocity", nav.velocity.magnitude);
        transform.position = Vector3.MoveTowards(transform.position, moveSpots.position, moveSpeed * Time.deltaTime);
     

        if (Vector3.Distance(transform.position, moveSpots.position) < 0.2f)
        {
            if(waitTime <= 0)
            {
                moveSpots.position = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY));
                waitTime = startWaitTime; 
            } else
            {
                waitTime -= Time.deltaTime;
            }
        }

    }

}
