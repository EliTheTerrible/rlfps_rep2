﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class AITry : MonoBehaviour
{
    [SerializeField] private float _attackRange = 3f;
    [SerializeField] private float _rayDIstance = 5.0f;
    [SerializeField] private float _stoppingDistance = 1.5f;
    [SerializeField] private float _moveSpeed = 5.0f;

                    private int destPoint = 0;
                    private NavMeshAgent agent;

    [SerializeField] private int lifePoint = 5;

    [SerializeField] private LayerMask _layerMask;

    private Vector3     _destination ;
    private Quaternion _desiredRotation;
    private Vector3 _direction;
    private AITry _target;
    private State _currentState;


    public Transform target;
    public Transform[] points;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.autoBraking = false;
    }
    private void Update()
    {   
           switch (_currentState)
           {
               case State.Patrol:
                   {
                   
                    if (NeedsDestination() && agent.remainingDistance < 0.5f )
                       
                    {
                        CheckForAggro();
                        GetDestination();
                        
                    }
                  

                    var rayColor = IsPathBlocked() ? Color.red : Color.green;
                    Debug.DrawRay(start: transform.position, dir: _direction * _rayDIstance, rayColor);

                    while (IsPathBlocked())
                    {
                        Debug.Log(message: "Path Blocked");
                        GetDestination();

                    }

                    CheckForAggro();
                    if (_target != null)
                    {
                        _target = GetComponent<AITry>();
                        _currentState = State.Chase;
                    }
                    break;
                }
            case State.Chase:
                {
                    if (_target == null)
                    {
                        _currentState = State.Patrol;
                        return;
                    }

                    transform.LookAt(_target.transform);
                    transform.Translate(translation: Vector3.forward * Time.deltaTime * _moveSpeed);

                    if (Vector3.Distance(a: transform.position, b: _target.transform.position) < _attackRange)
                    {
                        _currentState = State.Attack;
                    }
                    break;
                }

            case State.Attack:
                {
                    if (_target != null)
                    {
                        Debug.Log("Attacking target");
                    }
                    _currentState = State.Patrol;
                    break;
                }
        }
    }

    private bool IsPathBlocked()
    {
        Ray ray = new Ray(origin: transform.position, _direction);
        var hitSomething = Physics.RaycastAll(ray, _rayDIstance, _layerMask);
        return false; // hitSomething.Any(); 

    }
    private void GetDestination()
    {
        if (!agent.pathPending && agent.remainingDistance < 0.5f)
        {
            agent.destination = points[destPoint].position;
            transform.rotation = _desiredRotation;
            transform.Translate(translation: Vector3.forward * Time.deltaTime * _moveSpeed);

            destPoint = (destPoint + 1) % points.Length;
        }
        else
            return;
       

    }
    private bool NeedsDestination()
    {
        if (_destination == Vector3.zero)
            return true;

        var distance = Vector3.Distance(a: transform.position, b: _destination);
        if (distance <= _stoppingDistance)
            return true;

        return false;
    }
    Quaternion startingAngle = Quaternion.AngleAxis(angle: -60, Vector3.forward);
    Quaternion steptingAngle = Quaternion.AngleAxis(angle: 5, Vector3.forward);

    private void CheckForAggro()
    {
        float aggroRadius = 5f;

        RaycastHit hit;
        var angle = transform.rotation * startingAngle;
        var direction = Vector3.forward;
        var pos = transform.position;


        for (var i = 0; i < 24; i++)
        {
            if (Physics.Raycast(origin: pos, direction, out hit, aggroRadius))
            {
                var targt = hit.collider.GetComponent<FirstPersonController>();
                float dist = Vector3.Distance(target.position, transform.position);

                if (targt != null)
                {
                    Debug.DrawRay(start: pos, dir: direction * hit.distance, Color.red);

                }
                else if (targt != null && dist > 10)
                {
                    Debug.DrawRay(start: pos, dir: direction * hit.distance, Color.yellow);
                }
                else if (targt == null)
                {
                    Debug.DrawRay(start: pos, dir: direction * aggroRadius, Color.white);
                }
                 direction = steptingAngle * direction;
            }

        }
    }

    public enum State
    {
        Patrol,
        Chase,
        Attack,
        Hit,
        MovingDamaged,
        Dead
    }

}
