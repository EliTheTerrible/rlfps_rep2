﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPoolingSystem : MonoBehaviour
{
    public static AmmoPoolingSystem instance;
    public List<BulletImpact> akBullets; 
    
    public static AmmoPoolingSystem Instance { get { return instance; } }
    public  GameObject bulletPrefab;

    public int initialPoolSize = 20;
    void Awake()
    {
        if(instance != this) {
            if(instance != null) {
                Destroy(instance.gameObject);
            }
            instance = this;    
        }

        akBullets = new List<BulletImpact>(initialPoolSize);
        for (int i = 0; i < initialPoolSize; i++)
        {
            GameObject prefabInstance = Instantiate(bulletPrefab);
            prefabInstance.transform.SetParent(transform);
            prefabInstance.SetActive(false);
            akBullets.Add(prefabInstance.GetComponent<BulletImpact>());
        }
    }

   public BulletImpact GetBullet()
    {
        foreach (BulletImpact bullet in akBullets)
        {
            if (!bullet.gameObject.activeInHierarchy)
            {
                bullet.gameObject.SetActive(true);                
                return bullet; 
            }
        }

        GameObject newBullet = Instantiate(bulletPrefab, transform);
        var newBulletImpact = newBullet.GetComponent<BulletImpact>();
        akBullets.Add(newBulletImpact);
        return newBulletImpact; 
    }
}
