﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Events;

public class Attack_Animation_System : MonoBehaviour
{   
   [SerializeField] Animator anim;
    float m_HorizontalMovement = 1f;
    float speed; 
    //Unity Events
    public UnityEvent WeaponActivated = new UnityEvent ();
    
    //Animation System
    public static Attack_Animation_System current;
 
    //Enum
    public enum E_Weapon_Selection 
    {
        Melee, HandGun, AssaulRifle, SawedOff
    }
    //References
    public E_Weapon_Selection E_SelectedWeapon;
        
    void Start()
    {   
       anim = GetComponent<Animator>(); 
       if (current != this)
       {
           
           if (current != null)
           {
               Destroy(current.gameObject); 
           }
           current = this; 
       }
      
    }
    
    void Update()
    {  
     SwitchWeaponLayer(); 
     if (WeaponActivated != null)
     {
   WeaponActivated.Invoke();
    }
        if(Input.GetAxis("Horizontal") !=0 || Input.GetAxis("Vertical") !=0)
        
        {

            anim.SetBool("isWalking", true);
            if (Input.GetKey(KeyCode.LeftShift))
            {   speed += Mathf.Lerp(0f, 1f, Time.deltaTime);
                speed=  Mathf.Clamp01(speed); 
                anim.SetBool("isSprinting", true);
                anim.SetFloat("Speed", speed); 
            }

            else
            { anim.SetBool("isSprinting", false);
                speed = 0f;
                anim.SetFloat("Speed", 0f);
            }
        }
        else
        {
            anim.SetBool("isWalking", false);
          
        }
       
       

    }

  

    UnityEvent WeaponMelee ()
{
            
            anim.SetBool ("meleeEquipped",true);
            anim.SetBool ("gunEquipped",false);
            anim.SetBool ("akEquipped",false);
            anim.SetBool ("SOEquipped", false); 
        
        
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
             anim.SetBool ("swinging2",true) ;
            anim.SetBool ("swinging1",false) ; 
            
        } 
        else if (!Input.GetKeyDown(KeyCode.Mouse0))
        {
              anim.SetBool ("swinging2",false) ;
           
        }


         if (Input.GetKeyDown(KeyCode.Mouse1))
             {
          
            anim.SetBool ("swinging1",true) ; 
            anim.SetBool ("swinging2",false) ; 
         
             }
        else if (!Input.GetKeyDown(KeyCode.Mouse1))
        {
              anim.SetBool ("swinging1",false) ;
        }
    return null; 

}

UnityEvent GunEquipped ()
{   
   
        anim.SetBool ("gunEquipped", true); 
        anim.SetBool ("meleeEquipped",false);
        anim.SetBool ("akEquipped",false);
        anim.SetBool ("SOEquipped", false);
        if (Input.GetKey(KeyCode.Mouse1))
        {

            anim.SetBool("isAiming", true);

        }
        else if (!Input.GetKey(KeyCode.Mouse1))
        {

            anim.SetBool("isAiming", false);
        }
        if (Input.GetKey(KeyCode.Mouse0))
        {
            anim.SetBool("isFiring", true);
        }
        else if (!Input.GetKey(KeyCode.Mouse0))
        {
            anim.SetBool("isFiring", false);
        }

       
        return null;
    }

UnityEvent AKEquipped (){
     

        anim.SetBool ("akEquipped", true); 
        anim.SetBool ("gunEquipped",false);
        anim.SetBool ("meleeEquipped",false); 
        anim.SetBool ("SOEquipped", false); 
    
     if  (Input.GetKey(KeyCode.Mouse1))
    {
        
        anim.SetBool ("isAiming",true); 
        
    } 
    else if  (!Input.GetKey(KeyCode.Mouse1))
    {

        anim.SetBool ("isAiming",false); 
    }

   if (Input.GetKey(KeyCode.Mouse0))
   {
            anim.SetBool("isFiring", true);
   }
   else if (!Input.GetKey(KeyCode.Mouse0))
   {
            anim.SetBool("isFiring", false);
   }

  
        return null ; 

}

UnityEvent SOEquipped (){
     

        anim.SetBool ("SOEquipped", true); 
        anim.SetBool ("gunEquipped",false);
        anim.SetBool ("akEquipped",false);
        anim.SetBool ("meleeEquipped",false);

        if (Input.GetKey(KeyCode.Mouse1))
        {

            anim.SetBool("isAiming", true);

        }
        else if (!Input.GetKey(KeyCode.Mouse1))
        {

            anim.SetBool("isAiming", false);
        }
        if (Input.GetKey(KeyCode.Mouse0))
        {
            anim.SetBool("isFiring", true);
        }
        else if (!Input.GetKey(KeyCode.Mouse0))
        {
            anim.SetBool("isFiring", false);
        }

        return null;

    }
public void WeaponSwitchEnum ( E_Weapon_Selection selectedWeapon) 
{   
    E_SelectedWeapon = selectedWeapon; 
    SwitchWeaponLayer(); 
}
public void SwitchWeaponLayer (){

    if (E_SelectedWeapon == E_Weapon_Selection.Melee)
    {
     
    WeaponActivated = WeaponMelee();

    anim.SetLayerWeight (anim.GetLayerIndex("Melee Attack Layer"), 1f); 
   

    anim.SetLayerWeight (anim.GetLayerIndex ("SO Layer"), 0f); 
    anim.SetLayerWeight (anim.GetLayerIndex("Gun Layer"), 0f); 
    anim.SetLayerWeight (anim.GetLayerIndex ("Ak Layer"), 0f); 
    }
   
    else if ( E_SelectedWeapon == E_Weapon_Selection.HandGun)
   { 

   
   WeaponActivated = GunEquipped(); 
   anim.SetLayerWeight (anim.GetLayerIndex ("Gun Layer"), 1f); 

   anim.SetLayerWeight (anim.GetLayerIndex ("SO Layer"), 0f); 
   anim.SetLayerWeight (anim.GetLayerIndex ("Melee Attack Layer"), 0f);
   anim.SetLayerWeight (anim.GetLayerIndex ("Ak Layer"), 0f); 

   }
   else if ( E_SelectedWeapon == E_Weapon_Selection.AssaulRifle)
   { 
    
    WeaponActivated = AKEquipped();
    anim.SetLayerWeight (anim.GetLayerIndex ("Ak Layer"), 1f); 

    anim.SetLayerWeight (anim.GetLayerIndex ("SO Layer"), 0f); 
    anim.SetLayerWeight (anim.GetLayerIndex ("Melee Attack Layer"), 0f);
    anim.SetLayerWeight (anim.GetLayerIndex("Gun Layer"), 0f); 

    }
     else if ( E_SelectedWeapon == E_Weapon_Selection.SawedOff)
   { 

   
   WeaponActivated = SOEquipped(); 
   anim.SetLayerWeight (anim.GetLayerIndex ("SO Layer"), 1f); 


   anim.SetLayerWeight (anim.GetLayerIndex ("Melee Attack Layer"), 0f);
    anim.SetLayerWeight (anim.GetLayerIndex ("Ak Layer"), 0f); 
    anim.SetLayerWeight (anim.GetLayerIndex ("Gun Layer"), 0f); 

   }

}
    
}

