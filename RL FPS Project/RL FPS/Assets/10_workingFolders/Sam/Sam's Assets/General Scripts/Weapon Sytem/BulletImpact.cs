﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletImpact :  MonoBehaviour
{
    public Rigidbody rb;
    public GameObject collisionExplosion;
    public float dieTime = 5f;

    //LifeTime
    public float lifeDuration = 2f;
    private float lifeTimer;
    private void OnEnable()
    {
        lifeTimer = lifeDuration;
        if (rb == null)
            rb = GetComponent<Rigidbody>();
        if (rb == null)
            Debug.Log("rigidbody not found!");

    }


    void Update()
    {

        lifeTimer -= Time.deltaTime;
        if (lifeTimer <= 0f)
        {
            Despawn(true);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "Player") {            
            Despawn(false);
        }
    }

    void Despawn(bool _timeOut) {
        if (AmmoPoolingSystem.instance != null)
            transform.SetParent(AmmoPoolingSystem.instance.transform);
        gameObject.SetActive(false);
        if (!_timeOut) {
            GameObject explosion = (GameObject)Instantiate(collisionExplosion, transform.position, transform.rotation, null);
            Destroy(explosion, dieTime);
        }
    }

}
