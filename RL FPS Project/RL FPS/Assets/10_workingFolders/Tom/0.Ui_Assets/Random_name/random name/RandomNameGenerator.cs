﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RandomNameGenerator : MonoBehaviour
{
    public TextMeshProUGUI LargeText;
    public TextMeshProUGUI oc;
    public TextMeshProUGUI Age;

    public void BtnAction()
    {
        PickRandomFromListname();
    }

    public void Randomocipaton()
    {
        PickRandomFromListocipation();
    }

    public void Rrandomage ()
    {
        RandomAgePicker(65);
    }


    private void PickRandomFromListname()
    {
        string[] studens = new string[] { "tom", "eli", "ari", "sam","Rosemary","Venita", "Zana", "Enid", "Simone", "Shani", "Ferne", "Tanja", "Taisha", "Carmelita", "Dorene", "Ilene", "Candelaria", "Delpha", "Franklin", "Brenton", "Angle", "Erin",
            "Vernice", "Soledad", "Alessandra", "Hanna", "Denisse", "Genia", "Shelly", "Malika", "Reita", "Hope", "Marisol", "Kay", "Adolfo", "Rossie", "Gertrud", "Karla", "Jule", "Twyla", "Erminia", "Peggy", "Sabrina", "Sunday", "Carolee", "Leisa",
            "Contessa", "Beatrice", "Fermina", "Marvis", "Melvin", "Diana", "Samatha", "Margit" };
        string randomName = studens[Random.Range(0, studens.Length)];
        LargeText.text = randomName;
    }

    private void PickRandomFromListocipation()
    {
        string[] studens = new string[] { "Maintenance & Repair Worker",
    "Musician",
    "Statistician",
    "Coach",
    "Anthropologist",
    "Truck Driver",
    "Social Worker",
    "Librarian",
    "Market Research Analyst",
    "Reporter",
    "Mathematician",
    "Financial Advisor",
    "Cashier",
    "Police Officer",
    "Veterinarian",
    "Civil Engineer",
    "Respiratory Therapist",
    "Physical Therapist",
    "School Counselor",
    "Preschool Teacher",
    "Surveyor",
    "Desktop publivsher",
    "Lawyer",
    "Drafter",
    "Sports Coach",
    "Security Guard",
    "Database administrator",
    "Art Director",
    "Childcare worker",
    "Budget analyst",
    "Real Estate Agent",
    "Physician",
    "Referee",
    "Dentist",
    "Fitness Trainer",
    "Computer Programmer",
    "Actuary",
    "Janitor",
    "Bookkeeping clerk",
    "Recreation & Fitness Worker",
    "Computer Systems Analyst",
    "Actor",
    "Electrical Engineer",
    "Economist",
    "Event Planner",
    "Hairdresser",
    "Web Developer",
    "Designer",
    "Loan Officer",
    "Food Scientist",
 };
        string randomName = studens[Random.Range(0, studens.Length)];
        oc.text = randomName;
    }
    private void RandomAgePicker(int maxint)
    {
        int randonmNum = Random.Range(20, maxint + 1);
        Age.text = randonmNum.ToString();
    }
}


