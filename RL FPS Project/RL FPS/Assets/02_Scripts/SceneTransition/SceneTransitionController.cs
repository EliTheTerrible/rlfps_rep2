﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneTransitionController : MonoBehaviour
{
    List<GameObject> DDOLobjects = new List<GameObject>();
    public static SceneTransitionController current;
    int randBackground;
    int randTip;
    public Sprite[] backgrounds;
    public string[] tips;
    public TMP_Text tipsText, debugText;
    public Image black, background;
    public RectTransform loadingBarMask, loadingBarFrame;
    public AnimationCurve curve;
    public float minimumLoadingScreenDisplayTime, blackoutHalftime;
    public bool forceMinimalLoadingTime;
    IEnumerator Start()
    {        
        OverideExistingInstance();
        StartCoroutine(FadeIn());
        yield return new WaitForSeconds(blackoutHalftime);
    }

    private void OverideExistingInstance()
    {
        if (current != this)
        {
            if (current != null)
            {
                Debug.Log("another instance of SceneTransitionController found");
                current.StopAllCoroutines();
                Destroy(current.gameObject);
            }
            current = this;
        }
    }

    public void StartTransion(string _sceneToLoadName, List<GameObject> objectToSave)
    {
        if(objectToSave != null)
        {
            foreach(GameObject OTS in objectToSave)
            {
                DontDestroyOnLoad(OTS);
                DDOLobjects.Add(OTS);
            }
        }
        StartCoroutine(Transition(_sceneToLoadName));
    }
    public IEnumerator Transition(string _sceneToLoadName)
    {
        Time.timeScale = 0f;
        DontDestroyOnLoad(this.gameObject);
        debugText.text = "";
        randBackground = Random.Range(0, backgrounds.Length);
        randTip = Random.Range(0, tips.Length);
        tipsText.text = tips[randTip];
        background.sprite = backgrounds[randBackground];
        background.gameObject.SetActive(false);

        StartCoroutine(FadeOut());
        yield return new WaitForSecondsRealtime(blackoutHalftime);

        background.gameObject.SetActive(true);
        StartCoroutine(FadeIn());
        yield return new WaitForSecondsRealtime(blackoutHalftime);



        AsyncOperation AO_loadLevel = SceneManager.LoadSceneAsync(_sceneToLoadName);
        AO_loadLevel.allowSceneActivation = false;
        if (!forceMinimalLoadingTime)
        {
            StartCoroutine(LoadingBarTrue(AO_loadLevel));
            yield return new WaitUntil(() => (AO_loadLevel.progress >= 0.9f));
        }
        else
        {
            StartCoroutine(LoadingBarDelayed(AO_loadLevel));
            yield return new WaitForSeconds(minimumLoadingScreenDisplayTime);
        }

        yield return new WaitUntil(() => AO_loadLevel.progress >= 0.9f);

        OverideExistingInstance();
        StartCoroutine(FadeOut());
        yield return new WaitForSecondsRealtime(blackoutHalftime);

        AO_loadLevel.allowSceneActivation = true;
        background.gameObject.SetActive(false);
        StartCoroutine(FadeIn());
        yield return new WaitForSecondsRealtime(blackoutHalftime);
        MergeSceneGameObject(_sceneToLoadName);
        debugText.text = "";
        Time.timeScale = 1f;
    }

    private void MergeSceneGameObject(string _sceneToLoadName)
    {
        Scene loadedScene = SceneManager.GetSceneByName(_sceneToLoadName);
        SceneManager.MoveGameObjectToScene(gameObject, loadedScene);
        foreach (GameObject objectToMove in DDOLobjects)
        {
            SceneManager.MoveGameObjectToScene(objectToMove, loadedScene);
        }
    }

    public IEnumerator TransitionToMainMenu()
    {        
        StartCoroutine(FadeOut());
        yield return new WaitForSecondsRealtime(blackoutHalftime);        
                
        AsyncOperation AO_loadLevel = SceneManager.LoadSceneAsync("MainMenu");
        AO_loadLevel.allowSceneActivation = true;
        yield return new WaitUntil(() => AO_loadLevel.isDone);
        
        OverideExistingInstance();
        StartCoroutine(FadeIn());
        yield return new WaitForSecondsRealtime(blackoutHalftime);        
    }

    IEnumerator FadeIn()
    {
        float t = blackoutHalftime;
        black.color = new Color(0f, 0f, 0f, 1f);
        while(t > 0f)
        {
            float a = curve.Evaluate(t/blackoutHalftime);
            black.color = new Color(0f, 0f, 0f, a);
            t -= Time.unscaledDeltaTime;
            yield return 0;     //Enumerator feature, skips to the next frame before contiuing
        }
        black.color = new Color(0f, 0f, 0f, 0f);        
    }
    IEnumerator FadeOut()
    {
        float t = 0f;
        black.color = new Color(0f, 0f, 0f, 0f);
        while (t < blackoutHalftime)
        {
            float a = curve.Evaluate(t / blackoutHalftime);
            black.color = new Color(0f, 0f, 0f, a);
            t += Time.unscaledDeltaTime;
            yield return 0;     //Enumerator feature, skips to the next frame before contiuing
        }
        black.color = new Color(0f, 0f, 0f, 1f);
    }
    IEnumerator LoadingBarTrue(AsyncOperation _currentlyLoadingScene)
    {        
        float prog = _currentlyLoadingScene.progress;

        while (prog < 0.9f)
        {
            prog = _currentlyLoadingScene.progress;
            float width = Mathf.Lerp(0f, loadingBarFrame.rect.width, prog);
            loadingBarMask.sizeDelta = new Vector2(width, 0f);
            yield return 0f;
        }
        loadingBarMask.sizeDelta = new Vector2(loadingBarFrame.rect.width, 0f);                
    }
    IEnumerator LoadingBarDelayed(AsyncOperation _currentlyLoadingScene)
    {
        Time.timeScale = 1f;
        float t = 0;
        while (t < minimumLoadingScreenDisplayTime)
        {
            debugText.text = ("load animation in progress: " + _currentlyLoadingScene.progress*100f + "%");
            float width = Mathf.Lerp(0f, loadingBarFrame.rect.width, t / minimumLoadingScreenDisplayTime);
            loadingBarMask.sizeDelta = new Vector2(width, 0f);            
            t += Time.deltaTime;                                   
            yield return 0;
        }
        loadingBarMask.sizeDelta = new Vector2(loadingBarFrame.rect.width, 0f);
    }        
}
 