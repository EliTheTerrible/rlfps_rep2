﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManeger : MonoBehaviour
{
    float timer;
    public Text ScoreText;
 

    bool _IsGamePaused;
    public GameObject _PauseMenu;
  



    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        timer = 0f;
      
        Time.timeScale = 1f;

    }


    void Update()
    {
        timer += Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (_IsGamePaused == true)
            {
                _UnPause();
                Time.timeScale = 1f;
            }
            else { _Pause(); }
        }

    }

    public void Lose()
    {
        
    }
    public void ResetGame()
    {
        SceneManager.LoadSceneAsync(1);
    }
    public void BackToMM()
    {
        SceneManager.LoadSceneAsync (0);
    }


    void _Pause()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        _PauseMenu.SetActive(true);
        Time.timeScale = 0f;
        _IsGamePaused = true;
    }
    public void _UnPause()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        _PauseMenu.SetActive(false);
        Time.timeScale = 1f;
        _IsGamePaused = false;
    }
  


}